#!/bin/sh

JAVA_HOME="`/usr/libexec/java_home -v 1.8`"
TERM=xterm-color

jython -J-Xms6000m -J-Xmx6000m easyGW.py 
jython -J-Xms6000m -J-Xmx6000m hardGW.py 

python3 plots.csv
