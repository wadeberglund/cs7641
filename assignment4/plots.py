#!/usr/bin/env python3

"""
Create plots from csv data
"""

import os
import sys
import csv
import pickle
import pandas as pd
import matplotlib.pyplot as plt

def plot_series(csvs, col, legends, limit=None, title=None, xlabel=None, ylabel=None, color=True, outfile=None):
    assert(len(csvs) == len(legends))

    plt.figure()

    if not color:
        monochrome = (cycler('color', ['k', '0.8']) *
                      cycler('marker', ['', '.', 'o']) *
                      cycler('linestyle', ['-', '--', ':', '-.']))

        plt.rc('axes', prop_cycle=monochrome)

    for csv,legend in zip(csvs, legends):
        df = pd.read_csv(csv)
        s = df[col]
        s.name = legend
        if limit:
            plt.plot(s.index[:limit], s.values[:limit], label=legend)
        else:
            plt.plot(s.index, s.values, label=legend)

    if title:
        plt.title(title)

    if xlabel:
        plt.xlabel(xlabel)

    if ylabel:
        plt.ylabel(ylabel)

    if len(legends) > 1:
        plt.legend(frameon=True)

    plt.grid()

    if outfile:
        plt.savefig(outfile, dpi=300, bbox_inches='tight')
    else:
        plt.show()

# Value and policy iteration plots

plot_series(['csv/Easy_Value.csv', 'csv/Easy_Policy.csv', 'csv/Hard_Value.csv', 'csv/Hard_Policy.csv'],
            'convergence',
            ['Easy Value', 'Easy Policy', 'Hard Value', 'Hard Policy'],
            limit=50,
            title='Convergence',
            xlabel='Iteration',
            ylabel='Delta',
            outfile='csv/vpi_convergence.tiff')

plot_series(['csv/Easy_Value.csv', 'csv/Easy_Policy.csv', 'csv/Hard_Value.csv', 'csv/Hard_Policy.csv'],
            'time',
            ['Easy Value', 'Easy Policy', 'Hard Value', 'Hard Policy'],
            limit=50,
            title='Runtime',
            xlabel='Iteration',
            ylabel='Time (s)',
            outfile='csv/vpi_time.tiff')

plot_series(['csv/Easy_Value.csv', 'csv/Easy_Policy.csv', 'csv/Hard_Value.csv', 'csv/Hard_Policy.csv'],
            'reward',
            ['Easy Value', 'Easy Policy', 'Hard Value', 'Hard Policy'],
            limit=50,
            title='Reward',
            xlabel='Iteration',
            ylabel='Reward',
            outfile='csv/vpi_reward.tiff')

plot_series(['csv/Easy_Value.csv', 'csv/Easy_Policy.csv', 'csv/Hard_Value.csv', 'csv/Hard_Policy.csv'],
            'steps',
            ['Easy Value', 'Easy Policy', 'Hard Value', 'Hard Policy'],
            limit=50,
            title='Steps',
            xlabel='Iteration',
            ylabel='Steps',
            outfile='csv/vpi_steps.tiff')

# Q-learning plots
Q_csvs = []
Q_legends = []
for qInit in [0, 100]:
    for lr in [0.05, 0.1, 0.3]:
        # for epsilon in [0.1, 0.3, 0.5]:
        for epsilon in [0.5]:
            Q_csvs.append('csv/Easy_Q-Learning_L{:0.2f}_q{:0.1f}_E{:0.1f}.csv'.format(lr, qInit, epsilon))
            Q_legends.append(r'$Q_0$={:0.1f}, $\alpha$={:0.2f}, $\epsilon$={:0.1f}'.format(qInit, lr, epsilon))

plot_series(Q_csvs,
            'convergence',
            Q_legends,
            limit=1000,
            title='Convergence, Easy World',
            xlabel='Iteration',
            ylabel='Delta',
            outfile='csv/easy_q_convergence.tiff')

plot_series(Q_csvs,
            'reward',
            Q_legends,
            limit=1000,
            title='Average Reward, Easy World',
            xlabel='Iteration',
            ylabel='Average R(s)',
            outfile='csv/easy_q_reward.tiff')

plot_series(Q_csvs,
            'time',
            Q_legends,
            limit=1000,
            title='Runtime, Easy World',
            xlabel='Iteration',
            ylabel='Time (s)',
            outfile='csv/easy_q_time.tiff')

Q_csvs = []
Q_legends = []
for qInit in [0, 50]:
    for lr in [0.05, 0.1, 0.3]:
        for epsilon in [0.1, 0.3, 0.5]:
        # for epsilon in [0.3]:
            Q_csvs.append('csv/Hard_Q-Learning_L{:0.2f}_q{:0.1f}_E{:0.1f}.csv'.format(lr, qInit, epsilon))
            Q_legends.append(r'$Q_0$={:0.1f}, $\alpha$={:0.2f}, $\epsilon$={:0.1f}'.format(qInit, lr, epsilon))

plot_series(Q_csvs,
            'convergence',
            Q_legends,
            limit=10000,
            title='Convergence, Hard World',
            xlabel='Iteration',
            ylabel='Delta',
            outfile='csv/hard_q_convergence.tiff')

plot_series(Q_csvs,
            'reward',
            Q_legends,
            limit=10000,
            title='Average Reward, Hard World',
            xlabel='Iteration',
            ylabel='Average R(s)',
            outfile='csv/hard_q_reward.tiff')

plot_series(Q_csvs,
            'time',
            Q_legends,
            limit=10000,
            title='Runtime, Hard World',
            xlabel='Iteration',
            ylabel='Time (s)',
            outfile='csv/hard_q_time.tiff')

# Runtime
plot_series(['csv/Easy_Value.csv', 'csv/Easy_Policy.csv', 'csv/Easy_Q-Learning_L0.10_q100.0_E0.3.csv', 'csv/Hard_Value.csv', 'csv/Hard_Policy.csv', 'csv/Hard_Q-Learning_L0.05_q50.0_E0.3.csv'],
            'time',
            ['Easy Value', 'Easy Policy', 'Easy Q-Learning', 'Hard Value', 'Hard Policy', 'Hard Q-Learning'],
            limit=200,
            title='Runtime',
            xlabel='Iteration',
            ylabel='Time (s)',
            outfile='csv/all_runtime.tiff')

