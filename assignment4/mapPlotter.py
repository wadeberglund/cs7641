#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-4.git
"""

import sys
import pickle as pkl
import numpy as np
import matplotlib.pyplot as plt


infile = sys.argv[1]
words = infile.split('_')
print(words)

if 'Easy' in words:
    userMap = [
              [ 0,  0, 0, -1, 0, 0, 0, 0, -5, 0],
              [ 0,  1, 0,  1, 1, 1, 1, 1,  1, 0],
              [ 0,  1, 0,  1, 0, 0, 0, 0,  1, 0],
              [ 0,  1, 0,  1, 0, 0, 0, 0,  1, 0],
              [ 0,  1, 0,  1, 0, 0, 0, 0,  1, 0],
              [ 0,  1, 0,  1, 0, 0, 0, 0,  1, 0],
              [ 0,  1, 0,  1, 0, 0, 0, 0,  1, 0],
              [ 0,  1, 0,  1, 0, 0, 0, 0,  1, 0],
              [ 0,  1, 0,  1, 1, 1, 1, 1,  1, 0],
              [ 0, -3, 0,  0, 0, 0, 0, 0,  0, 0],
              ]
    dotSize = 4

if 'Hard' in words:
    userMap = [[ 0,  0,  0,  0,  0,  0, 0, 0,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0,  0,  0],
               [ 0,  0,  0,  0,  0,  0, 0, 0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0],
               [ 0,  0,  0,  0,  0,  0, 0, 0,  0,  0, -5,  0,  0,  0,  0,  0,  0,  0,  0,  0],
               [ 0,  0,  0,  0,  0,  0, 0, 0,  0,  0, -5,  0,  0,  0, -1, -1, -1, -1, -1, -1],
               [ 0,  0,  0,  0,  0,  0, 0, 0, -5, -5,  0, -5,  0,  0,  0,  0,  0,  0,  1,  0],
               [ 0,  0,  0,  0,  0,  0, 0, 0,  1,  0,  0,  0, -5,  0,  0,  0,  0,  1,  0,  0],
               [-3, -3, -5, -1, -5, -3, 0, 0,  0,  0,  0, -5,  0, -5,  0,  0,  1,  0,  0,  0],
               [ 0, -3, -3, -1, -3, -3, 1, 1,  0,  0, -5,  0,  0,  0,  0,  1,  0,  0,  0,  0],
               [ 0,  0,  0,  0,  0,  0, 0, 0,  1, -5,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0],
               [ 0,  0,  0,  1,  0,  0, 0, 0,  0,  1,  0,  0,  1,  1,  0,  0,  0,  0,  0,  0],
               [ 0,  0,  1,  0,  1,  0, 0, 0,  0,  0,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0],
               [ 0,  1,  0,  0,  0,  1, 0, 0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0],
               [ 0,  1, -5,  0,  0,  0, 1, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0],
               [ 0,  0, -5,  0,  0,  1, 0, 0,  0,  0,  0,  1, -5,  0,  1,  0,  0,  0,  0,  0],
               [ 0,  1, -5,  0,  1,  0, 0, 0,  0,  0,  1,  0,  0,  0,  1,  0,  0,  0, -1, -1],
               [ 0,  1,  0,  0,  0,  1, 0, 0,  0,  1,  0,  0,  0,  0,  1,  1,  1,  1, -1, -1],
               [ 0,  1,  0,  0,  0,  0, 0, 0,  1,  0,  0,  0,  0,  0,  1,  0,  0,  0, -1, -1],
               [ 0,  1,  0,  0,  0,  0, 0, 1,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0],
               [ 0,  1,  0,  0,  0,  0, 0, 1,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0],
               [ 0,  1,  0,  0,  0,  0, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0]]
    dotSize = 2


with open(sys.argv[1],'rb') as f:
    arr = pkl.load(f, encoding='latin1')

lookup = {'None': (0, 0),
          '>'   : (1, 0),
          'v'   : (0,-1),
          '^'   : (0, 1),
          '<'   : (-1,0)}

n = len(arr)
arr = np.array(arr)
X, Y = np.meshgrid(range(1,n+1), range(1,n+1))
U = X.copy()
V = Y.copy()
for i in range(n):
    for j in range(n):
        U[i,j] = lookup[arr[n-i-1,j]][0]
        V[i,j] = lookup[arr[n-i-1,j]][1]

plt.figure()
plt.title(sys.argv[2])

if len(sys.argv) > 3 and sys.argv[3] == '--map':
    for i in range(n):
        for j in range(n):
            if i == 0 and j == n-1:
                plt.text(j+1,
                         n-i,
                         'X',
                         horizontalalignment='center',
                         verticalalignment='center',
                         color='green')


            elif userMap[i][j] > 0:
                plt.text(j+1,
                         n-i,
                         '#',
                         horizontalalignment='center',
                         verticalalignment='center',
                         color='blue')

            elif userMap[i][j] < 0:
                plt.text(j+1,
                         n-i,
                         str(userMap[i][j]),
                         horizontalalignment='center',
                         verticalalignment='center',
                         color='red')
            else:
                plt.text(j+1,
                         n-i,
                         str(userMap[i][j]),
                         horizontalalignment='center',
                         verticalalignment='center',
                         color='black')
else:
    for i in range(n):
        for j in range(n):
            if userMap[i][j] > 0:
                plt.plot(j+1, n-i, '-ob', alpha=0.4, markersize=dotSize+abs(userMap[i][j]))
            if userMap[i][j] < 0:
                plt.plot(j+1, n-i, '-or', alpha=0.4, markersize=dotSize+abs(userMap[i][j]))

    Q = plt.quiver(X, Y, U, V, headaxislength=5, pivot='mid', angles='xy',  scale_units='xy', scale=1.5)

plt.xlim((0,n+1))
plt.ylim((0,n+1))
plt.savefig(sys.argv[1][:-4]+'.tiff', dpi=300, bbox_inches='tight')
plt.tight_layout()
plt.show()
