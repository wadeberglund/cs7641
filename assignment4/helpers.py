"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-4.git
"""

import os
import sys
import csv
import pickle

sys.path.append('./burlap.jar')

from burlap.assignment4.util.AnalysisRunner import calcRewardInEpisode

for outdir in ['pickle', 'csv']:
    if not os.path.isdir(outdir):
        os.mkdir(outdir)


def dumpCSV(nIter, times, rewards, steps, convergence, world, method):
    fname = 'csv/{}_{}.csv'.format(world, method)
    iters = range(1, nIter+1)
    assert len(iters) == len(times)
    assert len(iters) == len(rewards)
    assert len(iters) == len(steps)
    assert len(iters) == len(convergence)

    with open(fname, 'wb') as f:
        f.write('iter,time,reward,steps,convergence\n')
        writer = csv.writer(f,delimiter=',')
        writer.writerows(zip(iters, times, rewards, steps, convergence))


def appendCSV(iters, times, rewards, steps, world, method, ow):
    fname = 'csv/size_{}.csv'.format(method)
    assert len(iters) == len(times)
    assert len(iters) == len(rewards)
    assert len(iters) == len(steps)

    if not os.path.exists(fname) or ow:
        with open(fname,'wb') as f:
            f.write('iter,time,reward,steps,shape\n')

    with open(fname, 'ab') as f:
        writer = csv.writer(f,delimiter=',')
        writer.writerows(zip(iters, times, rewards, steps, world))


def runEvals(trials, initialState, rf, tf, plan, rewardL, stepL):
    r = []
    s = []
    for trial in range(trials):
        ea = plan.evaluateBehavior(initialState, rf, tf, 200);
        r.append(calcRewardInEpisode(ea))
        s.append(ea.numTimeSteps())
    rewardL.append(sum(r)/float(len(r)))
    stepL.append(sum(s)/float(len(s)))


def comparePolicies(policy1, policy2):
    assert len(policy1) == len(policy1)
    diffs = 0
    for k in policy1.keys():
        if policy1[k] != policy2[k]:
            diffs +=1
    return diffs


def mapPicture(javaStrArr):
    out = []
    for row in javaStrArr:
        out.append([])
        for element in row:
            out[-1].append(str(element))
    return out


def dumpPolicyMap(javaStrArr, fname):
    pic = mapPicture(javaStrArr)
    with open(fname,'wb') as f:
        pickle.dump(pic, f)

