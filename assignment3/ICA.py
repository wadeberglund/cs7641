#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-3.git
"""


import os
import sys

import pandas as pd
import numpy as np

from sklearn.decomposition import FastICA
from sklearn.cluster import KMeans as kmeans
from sklearn.mixture import GaussianMixture as GMM
from scipy.stats import kurtosis

from helpers import balanced_accuracy, load_data, cluster_and_plot, plot_tsne_2d, plot_tsne_3d, plot_DR_ANN, plot_cluster_feature

out = './ICA/'
if not os.path.isdir(out):
    os.mkdir(out)

np.random.seed(0)


################################################################################
# Load Data, makes splits

neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY, connect4X, connect4Y = load_data('./baseline/')


################################################################################
# Part 2, Explore dimensionality reduction

# Remove non-Gaussian features
df = pd.DataFrame(neutrino_trgX)
neutrino_kurt = df.kurt(axis=0)
neutrino_kurt = neutrino_kurt.reset_index()

neutrino_idx = neutrino_kurt.index[neutrino_kurt.iloc[:,1].abs() > 3].tolist()
neutrino_kurt.columns=['k','kurtosis']

# Print CSV
neutrino_kurt.sort_values(by='kurtosis', ascending=False, inplace=True)
neutrino_kurt = neutrino_kurt.reset_index()
neutrino_kurt = neutrino_kurt.drop(columns=['index'])
neutrino_kurt.to_csv(out+'neutrino_kurtosis.csv', header=True)


# Remove non-Gaussian features
df = pd.DataFrame(connect4X)
connect4_kurt = df.kurt(axis=0)
connect4_kurt = connect4_kurt.reset_index()

connect4_idx = connect4_kurt.index[connect4_kurt.iloc[:,1].abs() > 3].tolist()
connect4_kurt.columns=['k','kurtosis']

# Print CSV
connect4_kurt.sort_values(by='kurtosis', ascending=False, inplace=True)
connect4_kurt = connect4_kurt.reset_index()
connect4_kurt = connect4_kurt.drop(columns=['index'])
connect4_kurt.to_csv(out+'connect4_kurtosis.csv', header=True)



################################################################################
# Part 3, DR + cluster

ica = FastICA(random_state=5)
neutrino_trgXDR = ica.fit_transform(neutrino_trgX[:,neutrino_idx])

plot_tsne_2d(neutrino_trgXDR, neutrino_trgY, "Neutrino ICA TSNE", out+"neutrino_TSNE_2d.tiff")
# plot_tsne_3d(neutrino_trgXDR, neutrino_trgY, "Neutrino ICA TSNE", out+"neutrino_TSNE.tiff")


ica = FastICA(random_state=5)
connect4XDR = ica.fit_transform(connect4X[:,connect4_idx])

plot_tsne_2d(connect4XDR, connect4Y, "Connect Four ICA TSNE", out+"connect4_TSNE_2d.tiff")
# plot_tsne_3d(connect4XDR, connect4Y, "Connect Four ICA TSNE", out+"connect4_TSNE.tiff")


cluster_and_plot(neutrino_trgXDR, neutrino_trgY, connect4XDR, connect4Y, out)


km = kmeans(n_clusters=10, n_jobs=4, random_state=5)
plot_tsne_2d(neutrino_trgXDR, km.fit_predict(neutrino_trgXDR), "Neutrino ICA TSNE, k-means=10", out+'neutrino_TSNE_km.tiff')

gmm = GMM(n_components=10, random_state=5)
plot_tsne_2d(neutrino_trgXDR, gmm.fit_predict(neutrino_trgXDR), "Neutrino ICA TSNE, EM=10", out+'neutrino_TSNE_gmm.tiff')

km = kmeans(n_clusters=10, n_jobs=4, random_state=5)
plot_tsne_2d(connect4XDR, km.fit_predict(connect4XDR), "Connect Four ICA TSNE, k-means=10", out+'connect4_TSNE_km.tiff')

gmm = GMM(n_components=10, random_state=5)
plot_tsne_2d(connect4XDR, gmm.fit_predict(connect4XDR), "Connect Four ICA TSNE, EM=10", out+'connect4_TSNE_gmm.tiff')


################################################################################
# Parts 4 and 5, Fit ANN

# DR + ANN
ica = FastICA(random_state=5)
grid ={'dr__n_components': range(2,len(neutrino_idx))}
plot_DR_ANN(neutrino_trgXDR, neutrino_trgY, ica, grid, out)

# Clusters as features
df = pd.DataFrame(neutrino_trgX)
neutrino_kurt = df.kurt(axis=0)
neutrino_kurt = neutrino_kurt.reset_index()
neutrino_idx = neutrino_kurt.index[neutrino_kurt.iloc[:,1].abs() > 3].tolist()

ica = FastICA(random_state=5)
neutrino_trgXDR = ica.fit_transform(neutrino_trgX)
neutrino_tstXDR = ica.fit_transform(neutrino_tstX)


plot_cluster_feature(neutrino_trgXDR, neutrino_tstXDR, neutrino_trgY, neutrino_tstY, 40, 20, out)

