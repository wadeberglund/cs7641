#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-3.git
"""

import os
import sys
from collections import defaultdict
from time import clock

import pandas as pd
import numpy as np

from sklearn.cluster import KMeans as kmeans
from sklearn.mixture import GaussianMixture as GMM

from helpers import load_data, cluster_and_plot, plot_tsne_2d, plot_tsne_3d, plot_cluster_feature


out = './baseline/'
if not os.path.isdir(out):
    os.mkdir(out)


np.random.seed(0)


################################################################################
# Load Data, makes splits

neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY, connect4X, connect4Y = load_data(out)


################################################################################
# Clustering data for parts 1 and 3

cluster_and_plot(neutrino_trgX, neutrino_trgY, connect4X, connect4Y, out)


plot_tsne_2d(neutrino_trgX, neutrino_trgY, "Neutrino Baseline TSNE", out+"neutrino_TSNE_2d.tiff")
plot_tsne_2d(connect4X, connect4Y, "Connect Four Baseline TSNE", out+"connect4_TSNE_2d.tiff")

plot_tsne_3d(neutrino_trgX, neutrino_trgY, "Neutrino Baseline TSNE", out+"neutrino_TSNE.tiff")
plot_tsne_3d(connect4X, connect4Y, "Connect Four Baseline TSNE", out+"connect4_TSNE.tiff")


km = kmeans(n_clusters=10, n_jobs=4, random_state=5)
plot_tsne_2d(neutrino_trgX, km.fit_predict(neutrino_trgX), "Neutrino TSNE, k-means=10", out+'neutrino_TSNE_km.tiff')

gmm = GMM(n_components=10, random_state=5)
plot_tsne_2d(neutrino_trgX, gmm.fit_predict(neutrino_trgX), "Neutrino TSNE, EM=10", out+'neutrino_TSNE_gmm.tiff')

km = kmeans(n_clusters=10, n_jobs=4, random_state=5)
plot_tsne_2d(connect4X, km.fit_predict(connect4X), "Connect Four TSNE, k-means=10", out+'connect4_TSNE_km.tiff')

gmm = GMM(n_components=10, random_state=5)
plot_tsne_2d(connect4X, gmm.fit_predict(connect4X), "Connect Four TSNE, EM=10", out+'connect4_TSNE_gmm.tiff')

plot_cluster_feature(neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY, 20, 15, out)
