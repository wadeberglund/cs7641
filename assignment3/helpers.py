#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-3.git
"""

from collections import Counter, defaultdict
from time import clock

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import scipy.sparse as sps
from scipy.linalg import pinv

from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.cluster import KMeans as kmeans
from sklearn.manifold import TSNE, MDS, SpectralEmbedding, Isomap, LocallyLinearEmbedding
from sklearn.metrics import accuracy_score, make_scorer
from sklearn.metrics import adjusted_mutual_info_score as ami
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.mixture import GaussianMixture as GMM
from sklearn.model_selection import GridSearchCV
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.utils.class_weight import compute_sample_weight
import sklearn.model_selection as ms


nn_arch1 = [(h,)*l for l in [1,2,3] for h in [25,50,100]]
nn_arch2 = [(h,)*l for l in [1,2,3] for h in [42,84,168]]
# nn_arch= [(50,50),(50,),(25,),(25,25),(100,25,100)]
nn_reg = [10**-x for x in range(1,5)]

def balanced_accuracy(truth, pred):
    wts = compute_sample_weight('balanced', truth)
    return accuracy_score(truth, pred, sample_weight=wts)


def cluster_acc(Y, clusterLabels):
    assert (Y.shape == clusterLabels.shape)
    Y_count = Counter(Y)
    Y_weights = {}
    for k,v in Y_count.most_common():
        Y_weights[k] = v / len(Y)

    pred = np.empty_like(Y)
    for label in set(clusterLabels):
        mask = (clusterLabels == label)
        Y_masked = Y[mask]
        Y_masked_w = {}
        for k,v in Counter(Y_masked).most_common():
            Y_masked_w[k] = int(v / Y_weights[k])

        target = Counter(Y_masked_w).most_common(1)[0][0]
        pred[mask] = target
    return balanced_accuracy(Y, pred)


class myGMM(GMM):
    def transform(self,X):
        return self.predict_proba(X)


def pairwiseDistCorr(X1,X2):
    assert X1.shape[0] == X2.shape[0]

    d1 = pairwise_distances(X1)
    d2 = pairwise_distances(X2)
    return np.corrcoef(d1.ravel(),d2.ravel())[0,1]



def reconstructionError(projections,X):
    W = projections.components_
    if sps.issparse(W):
        W = W.todense()
    p = pinv(W)
    reconstructed = ((p@W)@(X.T)).T # Unproject projected data
    errors = np.square(X-reconstructed)
    return np.nanmean(errors)



# http://datascience.stackexchange.com/questions/6683/feature-selection-using-feature-importances-in-random-forests-with-scikit-learn
class ImportanceSelect(BaseEstimator, TransformerMixin):
    def __init__(self, model, n=1):
         self.model = model
         self.n = n
    def fit(self, *args, **kwargs):
         self.model.fit(*args, **kwargs)
         return self
    def transform(self, X):
         return X[:,self.model.feature_importances_.argsort()[::-1][:self.n]]
    # def fit_transform(self, X, *args, **kwargs):
    #      self.model.fit(*args, **kwargs)
    #      return X[:,self.model.feature_importances_.argsort()[::-1][:self.n]]



################################################################################
# Load Data, makes splits, create pipelines, fit data

def load_data(out):
    neutrino_raw = pd.read_csv(out+'MiniBooNE_PID.txt', sep='\s+', header=None)

    neutrino = neutrino_raw.sample(20000)
    neutrinoX = neutrino.iloc[:,:50].values.astype(np.float64)
    neutrinoX = StandardScaler().fit_transform(neutrinoX)
    neutrinoY = neutrino.iloc[:,50].values.astype(np.int)

    neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY = ms.train_test_split(neutrinoX,
                                                                                     neutrinoY,
                                                                                     test_size=0.2,
                                                                                     random_state=25,
                                                                                     stratify=neutrinoY)

    connect4 = pd.read_csv(out+'connect-4.data')
    connect4X = pd.get_dummies(connect4.loc[:,'a1':'g6'], drop_first=True).values.astype(np.float64)
    connect4['result'].replace('draw', 0, inplace=True)
    connect4['result'].replace('win',  1, inplace=True)
    connect4['result'].replace('loss', 2, inplace=True)
    connect4Y = connect4['result'].values.astype(np.int)

    return neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY, connect4X, connect4Y

################################################################################
# Clustering data for parts 1 and 3

def cluster_and_plot(trg1X, trg1Y, trg2X, trg2Y, out):
    clusters =  [2,5,10,15,20,25,30,35,40,45,50,60,70,80,90,100]
    # clusters =  [2,5,10,15,20,25,30,35,40,45,50]
    # clusters =  [2,5,10,15,20]
    # clusters =  [2,5]

    # Four distance measures, sum of squared error, log-likelihood, accuracy and mutual information
    SSE = defaultdict(dict)
    ll = defaultdict(dict)
    acc = defaultdict(lambda: defaultdict(dict))
    adjMI = defaultdict(lambda: defaultdict(dict))

    km = kmeans(random_state=5)
    gmm = GMM(random_state=5)

    print(out)
    print("k, Time")
    st = clock()
    for k in clusters:
        km.set_params(n_clusters=k, n_jobs=4)
        gmm.set_params(n_components=k)

        km.fit(trg1X)
        gmm.fit(trg1X)
        SSE['neutrino'][k] = km.score(trg1X)
        ll['neutrino'][k]  = gmm.score(trg1X)
        acc['neutrino']['kmeans'][k] = cluster_acc(trg1Y, km.predict(trg1X))
        acc['neutrino']['gmm'][k]    = cluster_acc(trg1Y, gmm.predict(trg1X))
        adjMI['neutrino']['kmeans'][k] = ami(trg1Y, km.predict(trg1X), average_method='arithmetic')
        adjMI['neutrino']['gmm'][k]    = ami(trg1Y, gmm.predict(trg1X), average_method='arithmetic')

        km.fit(trg2X)
        gmm.fit(trg2X)
        SSE['connect4'][k] = km.score(trg2X)
        ll['connect4'][k]  = gmm.score(trg2X)
        acc['connect4']['kmeans'][k] = cluster_acc(trg2Y, km.predict(trg2X))
        acc['connect4']['gmm'][k]    = cluster_acc(trg2Y, gmm.predict(trg2X))
        adjMI['connect4']['kmeans'][k] = ami(trg2Y, km.predict(trg2X), average_method='arithmetic')
        adjMI['connect4']['gmm'][k]    = ami(trg2Y, gmm.predict(trg2X), average_method='arithmetic')

        print(k, clock()-st)


    SSE = -pd.DataFrame(SSE)
    SSE.rename(columns = lambda x: x+' SSE', inplace=True)

    ll = pd.DataFrame(ll)
    ll.rename(columns = lambda x: x+' log-likelihood', inplace=True)

    acc   = pd.concat({k: pd.DataFrame(v) for k, v in acc.items()}, axis=0)
    adjMI = pd.concat({k: pd.DataFrame(v) for k, v in adjMI.items()}, axis=0)

    SSE.to_csv(out+'SSE.csv')
    ll.to_csv(out+'loglikelihood.csv')
    acc.loc['neutrino'].to_csv(out+'neutrino_acc.csv')
    acc.loc['connect4'].to_csv(out+'connect4_acc.csv')
    adjMI.loc['neutrino'].to_csv(out+'neutrino_adjMI.csv')
    adjMI.loc['connect4'].to_csv(out+'connect4_adjMI.csv')


def plot_tsne_2d(X, y, title=None, outfile=None):
    trgX, _, trgY, _ = ms.train_test_split(X,
                                           y,
                                           test_size=0.5,
                                           random_state=25,
                                           stratify=y)

    clf = TSNE(n_components=2, learning_rate=100, init='pca', random_state=0)

    X_clf = clf.fit_transform(trgX)

    # Only plot a few percent of them, but learn on the whole set
    trgX, _, trgY, _ = ms.train_test_split(X_clf,
                                           trgY,
                                           test_size=0.8,
                                           random_state=25,
                                           stratify=trgY)


    colorize = dict(c=trgY, cmap=plt.cm.get_cmap('rainbow', 10))
    plt.scatter(trgX[:, 0], trgX[:, 1], **colorize)

    if title is not None:
        plt.title(title)

    plt.axis('equal')
    plt.grid()

    if outfile is not None:
        plt.savefig(outfile, dpi=300, bbox_inches='tight')
    else:
        plt.show()


def plot_tsne_3d(X, y, title=None, outfile=None):
    trgX, _, trgY, _ = ms.train_test_split(X,
                                           y,
                                           test_size=0.5,
                                           random_state=25,
                                           stratify=y)

    n_neighbors = 30
    clf = TSNE(n_components=3, learning_rate=100, init='pca', random_state=0)
    # clf = Isomap(n_neightbors, n_components=3)
    # clf = MDS(n_components=3, n_init=1, max_iter=100, random_state=0)
    # clf = SpectralEmbedding(n_components=3, random_state=0, eigen_solver="arpack")
    # clf = LocallyLinearEmbedding(n_neighbors, n_components=3, method='standard')

    X_clf = clf.fit_transform(trgX)

    # Only plot a few percent of them, but learn on the whole set
    trgX, _, trgY, _ = ms.train_test_split(X_clf,
                                           trgY,
                                           test_size=0.95,
                                           random_state=25,
                                           stratify=trgY)


    colorize = dict(c=trgY, cmap=plt.cm.get_cmap('rainbow', 10))
    ax = plt.axes(projection='3d')
    ax.scatter3D(trgX[:, 0], trgX[:, 1], trgX[:, 2], **colorize);

    if title is not None:
        plt.title(title)

    plt.axis('equal')
    plt.grid()

    if outfile is not None:
        plt.savefig(outfile, dpi=300, bbox_inches='tight')
    else:
        plt.show()

def plot_DR_ANN(X, Y, dr, grid, out):
    scorer = make_scorer(balanced_accuracy)

    mlp = MLPClassifier(activation='relu',
                        alpha=1e-4,
                        hidden_layer_sizes=(150,150),
                        max_iter=2000,
                        early_stopping=True,
                        random_state=5)

    pipe = Pipeline([('dr', dr), ('ANN', mlp)])

    gs = GridSearchCV(pipe,
                      n_jobs=4,
                      param_grid=grid,
                      refit=True,
                      verbose=10,
                      cv=5,
                      scoring=scorer,
                      error_score=np.nan,
                      return_train_score=True)

    gs.fit(X, Y)
    tmp = pd.DataFrame(gs.cv_results_)
    tmp.to_csv(out+'neutrino_DR_ANN.csv')

def plot_cluster_feature(trgX, tstX, trgY, tstY, km, kc, out):
    # Store our results
    accuracy = []

    # ANN classifier
    mlp = MLPClassifier(activation='relu',
                        alpha=1e-4,
                        hidden_layer_sizes=(150,150),
                        max_iter=2000,
                        early_stopping=True,
                        random_state=5)

    onehot = OneHotEncoder(categories='auto')

    # 1. Baseline
    mlp.fit(trgX, trgY)
    pred = mlp.predict(tstX)
    acc = balanced_accuracy(tstY, pred)
    print(acc)
    accuracy.append(acc)


    ########################################
    # k-means
    km = kmeans(n_clusters=km, n_jobs=4, random_state=5)
    km.fit(trgX)
    # We have to combine all of these together before one-hot encoding and then break them back apart
    allX = np.vstack([trgX, tstX])
    allXkm = onehot.fit_transform(np.atleast_2d(km.predict(allX)).T).toarray()
    trgXkm = allXkm[:trgX.shape[0],:]
    tstXkm = allXkm[trgX.shape[0]:,:]


    # 2. Use k-means clusters as additional features
    trgX1 = np.hstack([trgX, trgXkm])
    tstX1 = np.hstack([tstX, tstXkm])
    mlp.fit(trgX1, trgY)
    pred = mlp.predict(tstX1)
    acc = balanced_accuracy(tstY, pred)
    print(acc)
    accuracy.append(acc)


    # 3. Use k-means clusters as only features
    mlp.fit(trgXkm, trgY)
    pred = mlp.predict(tstXkm)
    acc = balanced_accuracy(tstY, pred)
    print(acc)
    accuracy.append(acc)


    ########################################
    # EM
    gmm = GMM(n_components=kc, random_state=5)
    gmm.fit(trgX)
    allX = np.vstack([trgX, tstX])
    allXgmm = onehot.fit_transform(np.atleast_2d(gmm.predict(allX)).T).toarray()
    trgXgmm = allXgmm[:trgX.shape[0],:]
    tstXgmm = allXgmm[trgX.shape[0]:,:]


    # 4. Use EM components as additional features
    trgX1 = np.hstack([trgX, trgXgmm])
    tstX1 = np.hstack([tstX, tstXgmm])
    mlp.fit(trgX1, trgY)
    pred = mlp.predict(tstX1)
    acc = balanced_accuracy(tstY, pred)
    print(acc)
    accuracy.append(acc)


    # 5. Use EM components as only features
    mlp.fit(trgXgmm, trgY)
    pred = mlp.predict(tstXgmm)
    acc = balanced_accuracy(tstY, pred)
    print(acc)
    accuracy.append(acc)

    print('Baseline,k-means additional,k-means only,EM additional,EM only')
    print('{},{},{},{},{}'.format(*accuracy))

