#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-3.git
"""

import os
import sys

import pandas as pd
import numpy as np

from sklearn.decomposition import PCA
from sklearn.cluster import KMeans as kmeans
from sklearn.mixture import GaussianMixture as GMM

from helpers import balanced_accuracy, load_data, cluster_and_plot, plot_tsne_2d, plot_tsne_3d, plot_DR_ANN, plot_cluster_feature

out = './PCA/'
if not os.path.isdir(out):
    os.mkdir(out)

np.random.seed(0)

################################################################################
# Load Data, makes splits

neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY, connect4X, connect4Y = load_data('./baseline/')


################################################################################
# Part 2, Explore dimensionality reduction

pca = PCA(random_state=5)
pca.fit(neutrino_trgX)
tmp = pd.Series(data=pca.explained_variance_, index=range(1,51))
tmp.to_csv(out+'neutrino_variance.csv', header=True)


pca = PCA(random_state=5)
pca.fit(connect4X)
tmp = pd.Series(data=pca.explained_variance_, index=range(1,85))
tmp.to_csv(out+'connect4_variance.csv', header=True)


################################################################################
# Part 3, DR + cluster

dim = 8
pca = PCA(n_components=dim, random_state=5)
neutrino_trgXDR = pca.fit_transform(neutrino_trgX)

plot_tsne_2d(neutrino_trgXDR, neutrino_trgY, "Neutrino PCA TSNE", out+"neutrino_TSNE_2d.tiff")
# plot_tsne_3d(neutrino_trgXDR, neutrino_trgY, "Neutrino PCA TSNE", out+"neutrino_TSNE.tiff")


dim = 22
pca = PCA(n_components=dim, random_state=5)
connect4XDR = pca.fit_transform(connect4X)

plot_tsne_2d(connect4XDR, connect4Y, "Connect Four PCA TSNE", out+"connect4_TSNE_2d.tiff")
# plot_tsne_3d(connect4XDR, connect4Y, "Connect Four PCA TSNE", out+"connect4_TSNE.tiff")


cluster_and_plot(neutrino_trgXDR, neutrino_trgY, connect4XDR, connect4Y, out)


km = kmeans(n_clusters=10, n_jobs=4, random_state=5)
plot_tsne_2d(neutrino_trgXDR, km.fit_predict(neutrino_trgXDR), "Neutrino PCA TSNE, k-means=10", out+'neutrino_TSNE_km.tiff')

gmm = GMM(n_components=10, random_state=5)
plot_tsne_2d(neutrino_trgXDR, gmm.fit_predict(neutrino_trgXDR), "Neutrino PCA TSNE, EM=10", out+'neutrino_TSNE_gmm.tiff')

km = kmeans(n_clusters=10, n_jobs=4, random_state=5)
plot_tsne_2d(connect4XDR, km.fit_predict(connect4XDR), "Connect Four PCA TSNE, k-means=10", out+'connect4_TSNE_km.tiff')

gmm = GMM(n_components=10, random_state=5)
plot_tsne_2d(connect4XDR, gmm.fit_predict(connect4XDR), "Connect Four PCA TSNE, EM=10", out+'connect4_TSNE_gmm.tiff')

################################################################################
# Parts 4 and 5, Fit ANN

# DR + ANN
pca = PCA(random_state=5)
dims = [2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,45,50]
grid ={'pca__n_components': dims}
plot_DR_ANN(neutrino_trgX, neutrino_trgY, pca, grid, out)

# Clusters as features
dim = 15
pca = PCA(n_components=dim, random_state=5)
neutrino_trgXDR = pca.fit_transform(neutrino_trgX)
neutrino_tstXDR = pca.transform(neutrino_tstX)

plot_cluster_feature(neutrino_trgXDR, neutrino_tstXDR, neutrino_trgY, neutrino_tstY, 20, 35, out)
