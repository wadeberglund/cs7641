#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-3.git
"""


import os
import sys
from collections import defaultdict

import pandas as pd
import numpy as np

from sklearn.random_projection import SparseRandomProjection, GaussianRandomProjection
from sklearn.cluster import KMeans as kmeans
from sklearn.mixture import GaussianMixture as GMM
import sklearn.model_selection as ms

from helpers import pairwiseDistCorr, reconstructionError
from helpers import balanced_accuracy, load_data, cluster_and_plot, plot_tsne_2d, plot_tsne_3d, plot_DR_ANN, plot_cluster_feature

out = './RP/'
if not os.path.isdir(out):
    os.mkdir(out)

np.random.seed(0)

################################################################################
# Load Data, makes splits

neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY, connect4X, connect4Y = load_data('./baseline/')


################################################################################
# Part 2, Explore dimensionality reduction

ITERS = 4

pwdcs = {}
res = {}
for dim in range(2, neutrino_trgX.shape[1], 5):
# for dim in [2,5,10]:
    print(dim)
    pwdc = 0
    re = 0
    for i in range(ITERS):
        rp = SparseRandomProjection(random_state=i, n_components=dim)
        rp.fit(neutrino_trgX)
        re += reconstructionError(rp, neutrino_trgX)
        pwdc += pairwiseDistCorr(rp.transform(neutrino_trgX), neutrino_trgX)
    pwdcs[dim] = pwdc / ITERS
    res[dim] = re / ITERS

pwdcs = pd.DataFrame.from_dict(pwdcs, orient='index')
pwdcs.to_csv(out+'neutrino_pwdist.csv')
res = pd.DataFrame.from_dict(res, orient='index')
res.to_csv(out+'neutrino_re.csv')


connect4_trgX, connect4_tstX, connect4_trgY, connect4_tstY = ms.train_test_split(connect4X,
                                                                                 connect4Y,
                                                                                 test_size=0.7,
                                                                                 random_state=25,
                                                                                 stratify=connect4Y)


pwdcs = {}
res = {}
for dim in range(2, connect4X.shape[1], 5):
# for dim in [2,5,10]:
    print(dim)
    pwdc = 0
    re = 0
    for i in range(ITERS):
        rp = SparseRandomProjection(random_state=i, n_components=dim)
        rp.fit(connect4_trgX)
        re += reconstructionError(rp, connect4_trgX)
        pwdc += pairwiseDistCorr(rp.transform(connect4_trgX), connect4_trgX)
    pwdcs[dim] = pwdc / ITERS
    res[dim] = re / ITERS

pwdcs = pd.DataFrame.from_dict(pwdcs, orient='index')
pwdcs.to_csv(out+'connect4_pwdist.csv')
res = pd.DataFrame.from_dict(res, orient='index')
res.to_csv(out+'connect4_re.csv')


################################################################################
# Part 3, DR + cluster
dim = 12
rp = SparseRandomProjection(n_components=dim, random_state=5)
neutrino_trgXDR = rp.fit_transform(neutrino_trgX)

plot_tsne_2d(neutrino_trgXDR, neutrino_trgY, "Neutrino RP TSNE", out+"neutrino_TSNE_2d.tiff")
# plot_tsne_3d(neutrino_trgXDR, neutrino_trgY, "Neutrino RP TSNE", out+"neutrino_TSNE.tiff")

dim = 36
rp = SparseRandomProjection(n_components=dim, random_state=5)
connect4XDR = rp.fit_transform(connect4X)

plot_tsne_2d(connect4XDR, connect4Y, "Connect Four RP TSNE", out+"connect4_TSNE_2d.tiff")
# plot_tsne_3d(connect4XDR, connect4Y, "Connect Four RP TSNE", out+"connect4_TSNE.tiff")

cluster_and_plot(neutrino_trgXDR, neutrino_trgY, connect4XDR, connect4Y, out)

km = kmeans(n_clusters=10, n_jobs=4, random_state=5)
plot_tsne_2d(neutrino_trgXDR, km.fit_predict(neutrino_trgXDR), "Neutrino RP TSNE, k-means=10", out+'neutrino_TSNE_km.tiff')

gmm = GMM(n_components=10, random_state=5)
plot_tsne_2d(neutrino_trgXDR, gmm.fit_predict(neutrino_trgXDR), "Neutrino RP TSNE, EM=10", out+'neutrino_TSNE_gmm.tiff')

km = kmeans(n_clusters=10, n_jobs=4, random_state=5)
plot_tsne_2d(connect4XDR, km.fit_predict(connect4XDR), "Connect Four RP TSNE, k-means=10", out+'connect4_TSNE_km.tiff')

gmm = GMM(n_components=10, random_state=5)
plot_tsne_2d(connect4XDR, gmm.fit_predict(connect4XDR), "Connect Four RP TSNE, EM=10", out+'connect4_TSNE_gmm.tiff')


################################################################################
# Parts 4 and 5, Fit ANN

# DR + ANN
rp = SparseRandomProjection(random_state=5)
dims = [2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,45,50]
grid ={'dr__n_components': dims}
plot_DR_ANN(neutrino_trgX, neutrino_trgY, rp, grid, out)

# Clusters as features
dim = 15
rp = SparseRandomProjection(n_components=dim, random_state=5)
neutrino_trgXDR = rp.fit_transform(neutrino_trgX)
neutrino_tstXDR = rp.transform(neutrino_tstX)

plot_cluster_feature(neutrino_trgXDR, neutrino_tstXDR, neutrino_trgY, neutrino_tstY, 20, 70, out)
