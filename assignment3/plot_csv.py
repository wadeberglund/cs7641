#!/usr/bin/env python3

import argparse
from cycler import cycler
# import sys
# import os
# import re
# import math
# import random
# import copy
# import itertools
# import functools
# import unittest
# from collections import deque
# from collections import defaultdict
# from collections import Counter
# import heapq
# import sqlite3
# import numpy             as np
# import matplotlib        as mpl
import matplotlib.pyplot as plt
# import seaborn           as sns
# import networkx          as nx
import pandas            as pd
# improt scipy

# re.X  VERBOSE, whitespace is ignored
# re.M  MULTILINE, ^ and $ also match around \n, not just BOL and EOL
# re.S  DOTALL, . matches \n
# re_XMS = re.X | re.M | re.S



def plot_csv(csvs, cols, xaxis, title=None, xlabel=None, ylabel=None, color=True, outfile=None):
    '''
    Plot csv from the command line
    '''
    if not color:
        monochrome = (cycler('color', ['k', '0.8']) *
                      cycler('marker', ['', '.', 'o']) *
                      cycler('linestyle', ['-', '--', ':', '-.']))

        plt.rc('axes', prop_cycle=monochrome)

    for csv in csvs:
        for col in cols:
            plt.plot(xaxis, csv[col], label=col)

    if title:
        plt.title(title)

    if xlabel:
        plt.xlabel(xlabel)

    if ylabel:
        plt.ylabel(ylabel)

    plt.legend(frameon=True)
    plt.grid()

    if outfile:
        plt.savefig(outfile, dpi=300, bbox_inches='tight')

    plt.show()

    # plt.plot(x, y, label='Legend1')
    # plt.plot(x, y)

# Code the top-level code as a function for code reuse
if __name__ == '__main__':
    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Plot CSV files.')

    parser.add_argument('csv_files',
                        nargs='+',
                        help="input file as CSV")

    parser.add_argument('--cols',
                        help="columns to be plotted, default: all")

    parser.add_argument('--xaxis',
                        help="column to label x-axis, default: first col")

    parser.add_argument('--title',
                        help="graph title, default: ''")

    parser.add_argument('--xlabel',
                        help="x-axis label, default: ''")

    parser.add_argument('--ylabel',
                        help="y-axis label, default: ''")

    parser.add_argument('--average', action='store_true',
                        help="average the columns, default: %(default)s")

    parser.add_argument('--color', action='store_true',
                        help="plot in color, default: %(default)s")

    parser.add_argument('--outfile',
                        help="output file, default: %(default)s")

    args = parser.parse_args()

    print(args)

    # Read the CSV into a dataframe
    csvs = []
    for csv_file in args.csv_files:
        csv = pd.read_csv(csv_file)

        # Fix up missing arguments that can't be defaulted
        if args.cols is None:
            cols = csv.columns.tolist()[1:]
        else:
            cols = args.cols.split(',')

        if args.xaxis is None:
            xaxis = csv.iloc[:,0]
        else:
            xaxis = csv[args.xaxis]

        if args.average:
            csv['average'] = csv[cols].mean(axis=1)
            cols = ['average']

        csvs.append(csv)


    # Create an average column in the dataframe
    plot_csv(csvs,
             cols,
             xaxis,
             args.title,
             args.xlabel,
             args.ylabel,
             args.color,
             args.outfile)
