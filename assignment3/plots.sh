#!/bin/bash

# Baseline
./plot_csv.py --title "SSE for k-means" --xlabel "Clusters (k)" --ylabel "SSE" --outfile baseline/baseline_SSE.tiff baseline/SSE.csv
./plot_csv.py --title "Log-likelihood for EM" --xlabel "Components (k)" --ylabel "Log-likelihood" --outfile baseline/baseline_loglikelihood.tiff baseline/loglikelihood.csv
./plot_csv.py --title "Log-likelihood for EM" --cols 'neutrino log-likelihood'  --xlabel "Components (k)" --ylabel "Log-likelihood" --outfile baseline/baseline_neutrino_loglikelihood.tiff baseline/loglikelihood.csv
./plot_csv.py --title "Accuracy for neutrino" --xlabel "Clusters (k)" --ylabel "Accuracy (%)" --outfile baseline/baseline_neutrino_acc.tiff baseline/neutrino_acc.csv
./plot_csv.py --title "Accuracy for connect4" --xlabel "Clusters (k)" --ylabel "Accuracy (%)" --outfile baseline/baseline_connect4_acc.tiff baseline/connect4_acc.csv
./plot_csv.py --title "AMI for neutrino" --xlabel "Clusters (k)" --ylabel "AMI" --outfile baseline/baseline_neutrino_adjMI.tiff baseline/neutrino_adjMI.csv
./plot_csv.py --title "AMI for connect4" --xlabel "Clusters (k)" --ylabel "AMI" --outfile baseline/baseline_connect4_adjMI.tiff baseline/connect4_adjMI.csv

# PCA
./plot_csv.py --title "Neutrino Variance" --xlabel "Dimensions" --ylabel "Variance" --outfile "PCA/pca_neutrino_var.tiff" PCA/neutrino_variance.csv
./plot_csv.py --title "Connect Four Variance" --xlabel "Dimensions" --ylabel "Variance" --outfile "PCA/pca_connect4_var.tiff" PCA/connect4_variance.csv


# ICA
./plot_csv.py --title "Neutrino Kurtosis" --cols 'kurtosis' --xlabel "Dimensions" --ylabel "Kurtosis" --outfile "ICA/ica_neutrino_kurt.tiff" ICA/neutrino_kurtosis.csv
./plot_csv.py --title "Connect Four Kurtosis" --cols 'kurtosis' --xlabel "Dimensions" --ylabel "Kurtosis" --outfile "ICA/ica_connect4_kurt.tiff" ICA/connect4_kurtosis.csv


# RP
./plot_csv.py --title "Neutrino Pair-wise Distance" --xlabel "Dimensions" --ylabel "Pair-wise Distance" --outfile "RP/rp_neutrino_pwdist.tiff" RP/neutrino_pwdist.csv
./plot_csv.py --title "Connect Four Pair-wise Distance" --xlabel "Dimensions" --ylabel "Pair-wise Distance" --outfile "RP/rp_connect4_pwdist.tiff" RP/connect4_pwdist.csv


# RF
./plot_csv.py --title "Neutrino Feature Importance" --xlabel "Dimensions" --ylabel "Importance" --outfile "RF/rf_neutrino_fscore.tiff" RF/neutrino_fscore.csv
./plot_csv.py --title "Connect Four Feature Importance" --xlabel "Dimensions" --ylabel "Importance" --outfile "RF/rf_connect4_fscore.tiff" RF/connect4_fscore.csv


# All
plot_csv.py --title "Neutrino SSE for DR k-means" --xlabel "Clusters (k)" --ylabel "SSE" --cols "neutrino baseline,neutrino PCA,neutrino ICA,neutrino RP,neutrino RF" --outfile baseline/neutrino_dr_sse.tiff baseline/SSE.csv PCA/SSE.csv ICA/SSE.csv RP/SSE.csv RF/SSE.csv
plot_csv.py --title "Neutrino Log-likelihood for DR EM" --xlabel "Components (k)" --ylabel "Log-likelihood" --cols "neutrino baseline,neutrino PCA,neutrino ICA,neutrino RP,neutrino RF" --outfile baseline/neutrino_dr_loglikelihood.tiff baseline/loglikelihood.csv PCA/loglikelihood.csv ICA/loglikelihood.csv RP/loglikelihood.csv RF/loglikelihood.csv
plot_csv.py --title "Neutrino Accuracy for DR k-means" --xlabel "Clusters (k)" --ylabel "Accuracy" --cols "baseline kmeans,PCA kmeans,ICA kmeans,RP kmeans,RF kmeans" --outfile baseline/neutrino_dr_km_acc.tiff baseline/neutrino_acc.csv PCA/neutrino_acc.csv ICA/neutrino_acc.csv RP/neutrino_acc.csv RF/neutrino_acc.csv
plot_csv.py --title "Neutrino Accuracy for DR EM" --xlabel "Components (k)" --ylabel "Accuracy" --cols "baseline gmm,PCA gmm,ICA gmm,RP gmm,RF gmm" --outfile baseline/neutrino_dr_gmm_acc.tiff baseline/neutrino_acc.csv PCA/neutrino_acc.csv ICA/neutrino_acc.csv RP/neutrino_acc.csv RF/neutrino_acc.csv
plot_csv.py --title "Neutrino AMI for DR k-means" --xlabel "Clusters (k)" --ylabel "AMI" --cols "baseline kmeans,PCA kmeans,ICA kmeans,RP kmeans,RF kmeans" --outfile baseline/neutrino_dr_km_adjMI.tiff baseline/neutrino_adjMI.csv PCA/neutrino_adjMI.csv ICA/neutrino_adjMI.csv RP/neutrino_adjMI.csv RF/neutrino_adjMI.csv
plot_csv.py --title "Neutrino AMI for DR EM" --xlabel "Components (k)" --ylabel "AMI" --cols "baseline gmm,PCA gmm,ICA gmm,RP gmm,RF gmm" --outfile baseline/neutrino_dr_gmm_adjMI.tiff baseline/neutrino_adjMI.csv PCA/neutrino_adjMI.csv ICA/neutrino_adjMI.csv RP/neutrino_adjMI.csv RF/neutrino_adjMI.csv

plot_csv.py --title "Connect Four SSE for k-means" --xlabel "Clusters (k)" --ylabel "SSE" --cols "connect4 baseline,connect4 PCA,connect4 ICA,connect4 RP,connect4 RF" --outfile baseline/connect4_dr_sse.tiff baseline/SSE.csv PCA/SSE.csv ICA/SSE.csv RP/SSE.csv RF/SSE.csv
plot_csv.py --title "Connect Four Log-likelihood for EM" --xlabel "Components (k)" --ylabel "Log-likelihood" --cols "connect4 baseline,connect4 PCA,connect4 ICA,connect4 RP,connect4 RF" --outfile baseline/connect4_dr_loglikelihood.tiff baseline/loglikelihood.csv PCA/loglikelihood.csv ICA/loglikelihood.csv RP/loglikelihood.csv RF/loglikelihood.csv
plot_csv.py --title "Connect Four Accuracy for DR k-means" --xlabel "Clusters (k)" --ylabel "Accuracy" --cols "baseline kmeans,PCA kmeans,ICA kmeans,RP kmeans,RF kmeans" --outfile baseline/connect4_dr_km_acc.tiff baseline/connect4_acc.csv PCA/connect4_acc.csv ICA/connect4_acc.csv RP/connect4_acc.csv RF/connect4_acc.csv
plot_csv.py --title "Connect Four Accuracy for DR EM" --xlabel "Components (k)" --ylabel "Accuracy" --cols "baseline gmm,PCA gmm,ICA gmm,RP gmm,RF gmm" --outfile baseline/connect4_dr_gmm_acc.tiff baseline/connect4_acc.csv PCA/connect4_acc.csv ICA/connect4_acc.csv RP/connect4_acc.csv RF/connect4_acc.csv
plot_csv.py --title "Connect Four AMI for DR k-means" --xlabel "Clusters (k)" --ylabel "AMI" --cols "baseline kmeans,PCA kmeans,ICA kmeans,RP kmeans,RF kmeans" --outfile baseline/connect4_dr_km_adjMI.tiff baseline/connect4_adjMI.csv PCA/connect4_adjMI.csv ICA/connect4_adjMI.csv RP/connect4_adjMI.csv RF/connect4_adjMI.csv
plot_csv.py --title "Connect Four AMI for DR EM" --xlabel "Components (k)" --ylabel "AMI" --cols "baseline gmm,PCA gmm,ICA gmm,RP gmm,RF gmm" --outfile baseline/connect4_dr_gmm_adjMI.tiff baseline/connect4_adjMI.csv PCA/connect4_adjMI.csv ICA/connect4_adjMI.csv RP/connect4_adjMI.csv RF/connect4_adjMI.csv

plot_csv.py --title "Runtime for DR" --xlabel "Clusters (k)" --ylabel "time (s)" --cols "baseline,PCA,ICA,RP,RF" --outfile baseline/timing.tiff baseline/timing.csv PCA/timing.csv ICA/timing.csv RP/timing.csv RF/timing.csv
