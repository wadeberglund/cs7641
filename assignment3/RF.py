#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-3.git
"""


import os
import sys
from collections import defaultdict

import pandas as pd
import numpy as np
from matplotlib import cm



from sklearn.ensemble import RandomForestClassifier
from sklearn.cluster import KMeans as kmeans
from sklearn.mixture import GaussianMixture as GMM

from helpers import ImportanceSelect
from helpers import balanced_accuracy, load_data, cluster_and_plot, plot_tsne_2d, plot_tsne_3d, plot_DR_ANN, plot_cluster_feature

out = './RF/'
if not os.path.isdir(out):
    os.mkdir(out)


np.random.seed(0)

################################################################################
# Load Data, makes splits

neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY, connect4X, connect4Y = load_data('./baseline/')


################################################################################
# Part 2, Explore dimensionality reduction

rfc = RandomForestClassifier(n_estimators=100,
                             class_weight='balanced',
                             random_state=5,
                             n_jobs=4)

neutrino_fs = rfc.fit(neutrino_trgX, neutrino_trgY).feature_importances_
connect4_fs = rfc.fit(connect4X, connect4Y).feature_importances_

pd.Series(np.sort(neutrino_fs)[::-1]).to_csv(out+'neutrino_fscore.csv', header=True)
pd.Series(np.sort(connect4_fs)[::-1]).to_csv(out+'connect4_fscore.csv', header=True)


################################################################################
# Part 3, DR + cluster
dim = 13
filtr = ImportanceSelect(rfc, dim)
neutrino_trgXDR = filtr.fit_transform(neutrino_trgX, neutrino_trgY)

plot_tsne_2d(neutrino_trgXDR, neutrino_trgY, "Neutrino RF TSNE", out+"neutrino_TSNE_2d.tiff")
plot_tsne_3d(neutrino_trgXDR, neutrino_trgY, "Neutrino RF TSNE", out+"neutrino_TSNE.tiff")


dim = 55
filtr = ImportanceSelect(rfc, dim)
connect4XDR = filtr.fit_transform(connect4X, connect4Y)

plot_tsne_2d(connect4XDR, connect4Y, "Connect Four RF TSNE", out+"connect4_TSNE_2d.tiff")
plot_tsne_3d(connect4XDR, connect4Y, "Connect Four RF TSNE", out+"connect4_TSNE.tiff")


cluster_and_plot(neutrino_trgXDR, neutrino_trgY, connect4XDR, connect4Y, out)


km = kmeans(n_clusters=10, n_jobs=4, random_state=5)
plot_tsne_2d(neutrino_trgXDR, km.fit_predict(neutrino_trgXDR), "Neutrino RF TSNE, k-means=10", out+'neutrino_TSNE_km.tiff')

gmm = GMM(n_components=10, random_state=5)
plot_tsne_2d(neutrino_trgXDR, gmm.fit_predict(neutrino_trgXDR), "Neutrino RF TSNE, EM=10", out+'neutrino_TSNE_gmm.tiff')

km = kmeans(n_clusters=10, n_jobs=4, random_state=5)
plot_tsne_2d(connect4XDR, km.fit_predict(connect4XDR), "Connect Four RF TSNE, k-means=10", out+'connect4_TSNE_km.tiff')

gmm = GMM(n_components=10, random_state=5)
plot_tsne_2d(connect4XDR, gmm.fit_predict(connect4XDR), "Connect Four RF TSNE, EM=10", out+'connect4_TSNE_gmm.tiff')

################################################################################
# Parts 4 and 5, Fit ANN

# DR + ANN
filtr = ImportanceSelect(rfc)
dims = [2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,45,50]
grid ={'dr__n': dims}
plot_DR_ANN(neutrino_trgX, neutrino_trgY, filtr, grid, out)

# Clusters as features
dim = 20
filtr = ImportanceSelect(rfc, dim)
neutrino_trgXDR = filtr.fit_transform(neutrino_trgX, neutrino_trgY)
neutrino_tstXDR = filtr.transform(neutrino_tstX)

plot_cluster_feature(neutrino_trgXDR, neutrino_tstXDR, neutrino_trgY, neutrino_tstY, 20, 60, out)
