"""
Based on the original code in this repository:
https://github.com/pushkar/ABAGAIL.git

traveling salesman algorithm implementation in jython
This also prints the index of the points of the shortest route.
To make a plot of the route, write the points at these indexes 
to a file and plot them in your favorite tool.
"""

import sys
import os
import time

sys.path.append("./ABAGAIL/ABAGAIL.jar")

import java.io.FileReader as FileReader
import java.io.File as File
import java.lang.String as String
import java.lang.StringBuffer as StringBuffer
import java.lang.Boolean as Boolean
import java.util.Random as Random

import dist.DiscreteDependencyTree as DiscreteDependencyTree
import dist.DiscreteUniformDistribution as DiscreteUniformDistribution
import dist.Distribution as Distribution
import dist.DiscretePermutationDistribution as DiscretePermutationDistribution
import opt.DiscreteChangeOneNeighbor as DiscreteChangeOneNeighbor
import opt.EvaluationFunction as EvaluationFunction
import opt.GenericHillClimbingProblem as GenericHillClimbingProblem
import opt.HillClimbingProblem as HillClimbingProblem
import opt.NeighborFunction as NeighborFunction
import opt.RandomizedHillClimbing as RandomizedHillClimbing
import opt.SimulatedAnnealing as SimulatedAnnealing
import opt.example.FourPeaksEvaluationFunction as FourPeaksEvaluationFunction
import opt.ga.CrossoverFunction as CrossoverFunction
import opt.ga.SingleCrossOver as SingleCrossOver
import opt.ga.DiscreteChangeOneMutation as DiscreteChangeOneMutation
import opt.ga.GenericGeneticAlgorithmProblem as GenericGeneticAlgorithmProblem
import opt.ga.GeneticAlgorithmProblem as GeneticAlgorithmProblem
import opt.ga.MutationFunction as MutationFunction
import opt.ga.StandardGeneticAlgorithm as StandardGeneticAlgorithm
import opt.ga.UniformCrossOver as UniformCrossOver
import opt.prob.GenericProbabilisticOptimizationProblem as GenericProbabilisticOptimizationProblem
import opt.prob.MIMIC as MIMIC
import opt.prob.ProbabilisticOptimizationProblem as ProbabilisticOptimizationProblem
import shared.FixedIterationTrainer as FixedIterationTrainer
import opt.example.TravelingSalesmanEvaluationFunction as TravelingSalesmanEvaluationFunction
import opt.example.TravelingSalesmanRouteEvaluationFunction as TravelingSalesmanRouteEvaluationFunction
import opt.SwapNeighbor as SwapNeighbor
import opt.ga.SwapMutation as SwapMutation
import opt.example.TravelingSalesmanCrossOver as TravelingSalesmanCrossOver
import opt.example.TravelingSalesmanSortEvaluationFunction as TravelingSalesmanSortEvaluationFunction
import shared.Instance as Instance
import util.ABAGAILArrays as ABAGAILArrays

from array import array




"""
Commandline parameter(s):
    none
"""

if not os.path.isdir('./ro_output'):
    os.mkdir('./ro_output')

random = Random()
RUNS = 10


# Sweep N and report fitness and time, averaged across 10 runs
outfile = './ro_output/tsp_N.csv'
out = open(outfile, 'w')
out.write('N,RHC_fitness,RHC_time,SA_fitness,SA_time,GA_fitness,GA_time,MIMIC_fitness,MIMIC_time\n')

for N in [10, 50, 100, 150, 200]:
    out.write(str(N) + ',')

    points = [[0 for x in xrange(2)] for x in xrange(N)]
    for i in range(0, len(points)):
        points[i][0] = random.nextDouble()
        points[i][1] = random.nextDouble()

    ef = TravelingSalesmanRouteEvaluationFunction(points)
    odd = DiscretePermutationDistribution(N)
    nf = SwapNeighbor()
    mf = SwapMutation()
    cf = TravelingSalesmanCrossOver(ef)
    hcp = GenericHillClimbingProblem(ef, odd, nf)
    gap = GenericGeneticAlgorithmProblem(ef, odd, mf, cf)

    duration = 0
    fitness = 0
    for i in range(RUNS):
        rhc = RandomizedHillClimbing(hcp)
        fit = FixedIterationTrainer(rhc, 200000)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(rhc.getOptimal())
        print "RHC Inverse of Distance: " + str(ef.value(rhc.getOptimal()))
        # print "Route:"
        # path = []
        # for x in range(0,N):
        #     path.append(rhc.getOptimal().getDiscrete(x))
        # print path
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

    duration = 0
    fitness = 0
    for i in range(RUNS):
        sa = SimulatedAnnealing(1E12, .999, hcp)
        fit = FixedIterationTrainer(sa, 200000)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(sa.getOptimal())
        print "SA Inverse of Distance: " + str(ef.value(sa.getOptimal()))
        # print "Route:"
        # path = []
        # for x in range(0,N):
        #     path.append(sa.getOptimal().getDiscrete(x))
        # print path
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')


    duration = 0
    fitness = 0
    for i in range(RUNS):
        ga = StandardGeneticAlgorithm(2000, 1500, 250, gap)
        fit = FixedIterationTrainer(ga, 1000)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(ga.getOptimal())
        print "GA Inverse of Distance: " + str(ef.value(ga.getOptimal()))
        # print "Route:"
        # path = []
        # for x in range(0,N):
        #     path.append(ga.getOptimal().getDiscrete(x))
        # print path
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')


    duration = 0
    fitness = 0
    for i in range(RUNS):
        # for mimic we use a sort encoding
        ef = TravelingSalesmanSortEvaluationFunction(points);
        fill = [N] * N
        ranges = array('i', fill)
        odd = DiscreteUniformDistribution(ranges);
        df = DiscreteDependencyTree(.1, ranges); 
        pop = GenericProbabilisticOptimizationProblem(ef, odd, df);

        mimic = MIMIC(500, 100, pop)
        fit = FixedIterationTrainer(mimic, 1000)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(mimic.getOptimal())
        print "MIMIC Inverse of Distance: " + str(ef.value(mimic.getOptimal()))
        # print "Route:"
        # path = []
        # optimal = mimic.getOptimal()
        # fill = [0] * optimal.size()
        # ddata = array('d', fill)
        # for i in range(0,len(ddata)):
        #     ddata[i] = optimal.getContinuous(i)
        # order = ABAGAILArrays.indices(optimal.size())
        # ABAGAILArrays.quicksort(ddata, order)
        # print order
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS))

    out.write('\n')
out.close()

# Sweep number of iterations and report fitness and time, averaged across 10 runs
outfile = './ro_output/tsp_iter.csv'
out = open(outfile, 'w')
out.write('Iteration,RHC_fitness,RHC_time,SA_fitness,SA_time,GA_fitness,GA_time,MIMIC_fitness,MIMIC_time\n')

N = 50

points = [[0 for x in xrange(2)] for x in xrange(N)]
for i in range(0, len(points)):
    points[i][0] = random.nextDouble()
    points[i][1] = random.nextDouble()

ef = TravelingSalesmanRouteEvaluationFunction(points)
odd = DiscretePermutationDistribution(N)
nf = SwapNeighbor()
mf = SwapMutation()
cf = TravelingSalesmanCrossOver(ef)
hcp = GenericHillClimbingProblem(ef, odd, nf)
gap = GenericGeneticAlgorithmProblem(ef, odd, mf, cf)


for iterations in range(100,2001,100) + range(2100,100000,1000):
    out.write(str(iterations) + ',')

    duration = 0
    fitness = 0
    for i in range(RUNS):
        rhc = RandomizedHillClimbing(hcp)
        fit = FixedIterationTrainer(rhc, iterations)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(rhc.getOptimal())
        print "RHC Inverse of Distance: " + str(ef.value(rhc.getOptimal()))
        # print "Route:"
        # path = []
        # for x in range(0,N):
        #     path.append(rhc.getOptimal().getDiscrete(x))
        # print path
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

    duration = 0
    fitness = 0
    for i in range(RUNS):
        sa = SimulatedAnnealing(1E12, .999, hcp)
        fit = FixedIterationTrainer(sa, iterations)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(sa.getOptimal())
        print "SA Inverse of Distance: " + str(ef.value(sa.getOptimal()))
        # print "Route:"
        # path = []
        # for x in range(0,N):
        #     path.append(sa.getOptimal().getDiscrete(x))
        # print path
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')


    if iterations <= 2000:
        duration = 0
        fitness = 0
        for i in range(RUNS):
            ga = StandardGeneticAlgorithm(2000, 1500, 250, gap)
            fit = FixedIterationTrainer(ga, iterations)
            st = time.time()
            fit.train()
            duration += time.time() - st
            fitness += ef.value(ga.getOptimal())
            print "GA Inverse of Distance: " + str(ef.value(ga.getOptimal()))
            # print "Route:"
            # path = []
            # for x in range(0,N):
            #     path.append(ga.getOptimal().getDiscrete(x))
            # print path
        out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')


        duration = 0
        fitness = 0
        for i in range(RUNS):
            # for mimic we use a sort encoding
            ef = TravelingSalesmanSortEvaluationFunction(points);
            fill = [N] * N
            ranges = array('i', fill)
            odd = DiscreteUniformDistribution(ranges);
            df = DiscreteDependencyTree(.1, ranges); 
            pop = GenericProbabilisticOptimizationProblem(ef, odd, df);

            mimic = MIMIC(500, 100, pop)
            fit = FixedIterationTrainer(mimic, iterations)
            st = time.time()
            fit.train()
            duration += time.time() - st
            fitness += ef.value(mimic.getOptimal())
            print "MIMIC Inverse of Distance: " + str(ef.value(mimic.getOptimal()))
            # print "Route:"
            # path = []
            # optimal = mimic.getOptimal()
            # fill = [0] * optimal.size()
            # ddata = array('d', fill)
            # for i in range(0,len(ddata)):
            #     ddata[i] = optimal.getContinuous(i)
            # order = ABAGAILArrays.indices(optimal.size())
            # ABAGAILArrays.quicksort(ddata, order)
            # print order
        out.write(str(fitness/RUNS) + ',' + str(duration/RUNS))

    else:
        out.write('n/a,n/a,n/a,n/a')

    out.write('\n')
out.close()
