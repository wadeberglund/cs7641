#!/usr/bin/env python3

import numpy as np
import pandas as pd

import sklearn.model_selection as ms
from sklearn.preprocessing import MinMaxScaler, StandardScaler, OneHotEncoder

# Read neutrino data
neutrino_raw = pd.read_csv('MiniBooNE_PID.txt', sep='\s+', header=None)

neutrino = neutrino_raw.sample(100000)
neutrinoX = neutrino.iloc[:,:50].values.astype(np.float64)
neutrinoY = neutrino.iloc[:,50].values.astype(np.int)

trgX, tstX, trgY, tstY = ms.train_test_split(neutrinoX,
                                             neutrinoY,
                                             test_size=0.2,
                                             random_state=16,
                                             stratify=neutrinoY)

trgX, valX, trgY, valY = ms.train_test_split(trgX,
                                             trgY,
                                             test_size=0.2,
                                             random_state=16,
                                             stratify=trgY)

scaler = StandardScaler()
# scaler = MinMaxScaler()

trgX = scaler.fit_transform(trgX)
tstX = scaler.transform(tstX)
valX = scaler.transform(valX)

trgY = np.atleast_2d(trgY).T
tstY = np.atleast_2d(tstY).T
valY = np.atleast_2d(valY).T

trg = pd.DataFrame(np.hstack((trgX, trgY)))
tst = pd.DataFrame(np.hstack((tstX, tstY)))
val = pd.DataFrame(np.hstack((valX, valY)))

trg.to_csv('neutrino_trg.csv', index=False, header=False)
tst.to_csv('neutrino_tst.csv', index=False, header=False)
val.to_csv('neutrino_val.csv', index=False, header=False)

# # Read connect4 data
# connect4 = pd.read_csv('connect-4.data')
# connect4['result'].replace('draw', 0, inplace=True)
# connect4['result'].replace('win',  1, inplace=True)
# connect4['result'].replace('loss', 2, inplace=True)

# connect4X = pd.get_dummies(connect4.loc[:,'a1':'g6'], drop_first=True).values.astype(np.int)
# connect4Y = connect4['result'].values.astype(np.int)

# trgX, tstX, trgY, tstY = ms.train_test_split(connect4X,
#                                              connect4Y,
#                                              test_size=0.2,
#                                              random_state=17,
#                                              stratify=connect4Y)

# trgX, valX, trgY, valY = ms.train_test_split(trgX,
#                                              trgY,
#                                              test_size=0.2,
#                                              random_state=17,
#                                              stratify=trgY)

# trgY = np.atleast_2d(trgY).T
# tstY = np.atleast_2d(tstY).T
# valY = np.atleast_2d(valY).T

# # one_hot = OneHotEncoder(categories='auto')
# # trgY = one_hot.fit_transform(trgY.reshape(-1, 1)).todense()
# # tstY = one_hot.transform(tstY.reshape(-1, 1)).todense()
# # valY = one_hot.transform(valY.reshape(-1, 1)).todense()

# trg = pd.DataFrame(np.hstack((trgX, trgY)))
# tst = pd.DataFrame(np.hstack((tstX, tstY)))
# val = pd.DataFrame(np.hstack((valX, valY)))

# trg.to_csv('connect4_trg.csv', index=False, header=False)
# tst.to_csv('connect4_tst.csv', index=False, header=False)
# val.to_csv('connect4_val.csv', index=False, header=False)
