"""
Based on the original code in this repository:
https://github.com/pushkar/ABAGAIL.git

knapsack algorithm implementation in jython
"""

import sys
import os
import time

sys.path.append("./ABAGAIL/ABAGAIL.jar")

import java.io.FileReader as FileReader
import java.io.File as File
import java.lang.String as String
import java.lang.StringBuffer as StringBuffer
import java.lang.Boolean as Boolean
import java.util.Random as Random

import dist.DiscreteDependencyTree as DiscreteDependencyTree
import dist.DiscreteUniformDistribution as DiscreteUniformDistribution
import dist.Distribution as Distribution
import opt.DiscreteChangeOneNeighbor as DiscreteChangeOneNeighbor
import opt.EvaluationFunction as EvaluationFunction
import opt.GenericHillClimbingProblem as GenericHillClimbingProblem
import opt.HillClimbingProblem as HillClimbingProblem
import opt.NeighborFunction as NeighborFunction
import opt.RandomizedHillClimbing as RandomizedHillClimbing
import opt.SimulatedAnnealing as SimulatedAnnealing
import opt.example.FourPeaksEvaluationFunction as FourPeaksEvaluationFunction
import opt.ga.CrossoverFunction as CrossoverFunction
import opt.ga.SingleCrossOver as SingleCrossOver
import opt.ga.DiscreteChangeOneMutation as DiscreteChangeOneMutation
import opt.ga.GenericGeneticAlgorithmProblem as GenericGeneticAlgorithmProblem
import opt.ga.GeneticAlgorithmProblem as GeneticAlgorithmProblem
import opt.ga.MutationFunction as MutationFunction
import opt.ga.StandardGeneticAlgorithm as StandardGeneticAlgorithm
import opt.ga.UniformCrossOver as UniformCrossOver
import opt.prob.GenericProbabilisticOptimizationProblem as GenericProbabilisticOptimizationProblem
import opt.prob.MIMIC as MIMIC
import opt.prob.ProbabilisticOptimizationProblem as ProbabilisticOptimizationProblem
import shared.FixedIterationTrainer as FixedIterationTrainer
import opt.example.KnapsackEvaluationFunction as KnapsackEvaluationFunction
from array import array




"""
Commandline parameter(s):
    none
"""

if not os.path.isdir('./ro_output'):
    os.mkdir('./ro_output')

RUNS = 10

# Random number generator */
random = Random()


# # Sweep N and report fitness and time, averaged across 10 runs
# outfile = './ro_output/knapsack_N.csv'
# out = open(outfile, 'w')
# out.write('NUM_ITEMS,RHC_fitness,RHC_time,SA_fitness,SA_time,GA_fitness,GA_time,MIMIC_fitness,MIMIC_time\n')

# for NUM_ITEMS in [20,40,80,160,240,320,400]:
#     out.write(str(NUM_ITEMS) + ',')

#     # The number of copies each
#     COPIES_EACH = 4
#     # The maximum weight for a single element
#     MAX_WEIGHT = 50
#     # The maximum volume for a single element
#     MAX_VOLUME = 50
#     # The volume of the knapsack 
#     KNAPSACK_VOLUME = MAX_VOLUME * NUM_ITEMS * COPIES_EACH * .4

#     # create copies
#     fill = [COPIES_EACH] * NUM_ITEMS
#     copies = array('i', fill)

#     # create weights and volumes
#     fill = [0] * NUM_ITEMS
#     weights = array('d', fill)
#     volumes = array('d', fill)
#     for i in range(0, NUM_ITEMS):
#         weights[i] = random.nextDouble() * MAX_WEIGHT
#         volumes[i] = random.nextDouble() * MAX_VOLUME

#     # create range
#     fill = [COPIES_EACH + 1] * NUM_ITEMS
#     ranges = array('i', fill)


#     ef = KnapsackEvaluationFunction(weights, volumes, KNAPSACK_VOLUME, copies)
#     odd = DiscreteUniformDistribution(ranges)
#     nf = DiscreteChangeOneNeighbor(ranges)
#     mf = DiscreteChangeOneMutation(ranges)
#     cf = UniformCrossOver()
#     df = DiscreteDependencyTree(.1, ranges)
#     hcp = GenericHillClimbingProblem(ef, odd, nf)
#     gap = GenericGeneticAlgorithmProblem(ef, odd, mf, cf)
#     pop = GenericProbabilisticOptimizationProblem(ef, odd, df)

#     duration = 0
#     fitness = 0
#     for i in range(RUNS):
#         rhc = RandomizedHillClimbing(hcp)
#         fit = FixedIterationTrainer(rhc, 200000)
#         st = time.time()
#         fit.train()
#         duration += time.time() - st
#         fitness += ef.value(rhc.getOptimal())
#         print "RHC: " + str(ef.value(rhc.getOptimal()))
#     out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

#     duration = 0
#     fitness = 0
#     for i in range(RUNS):
#         sa = SimulatedAnnealing(100, .95, hcp)
#         fit = FixedIterationTrainer(sa, 200000)
#         st = time.time()
#         fit.train()
#         duration += time.time() - st
#         fitness += ef.value(sa.getOptimal())
#         print "SA: " + str(ef.value(sa.getOptimal()))
#     out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

#     duration = 0
#     fitness = 0
#     for i in range(RUNS):
#         ga = StandardGeneticAlgorithm(200, 150, 25, gap)
#         fit = FixedIterationTrainer(ga, 1000)
#         st = time.time()
#         fit.train()
#         duration += time.time() - st
#         fitness += ef.value(ga.getOptimal())
#         print "GA: " + str(ef.value(ga.getOptimal()))
#     out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

#     duration = 0
#     fitness = 0
#     for i in range(RUNS):
#         mimic = MIMIC(200, 100, pop)
#         fit = FixedIterationTrainer(mimic, 1000)
#         st = time.time()
#         fit.train()
#         duration += time.time() - st
#         fitness += ef.value(mimic.getOptimal())
#         print "MIMIC: " + str(ef.value(mimic.getOptimal()))
#     out.write(str(fitness/RUNS) + ',' + str(duration/RUNS))

#     out.write('\n')
# out.close()


# Sweep number of iterations and report fitness and time, averaged across 10 runs
outfile = './ro_output/knapsack_iter.csv'
out = open(outfile, 'w')
out.write('Iteration,RHC_fitness,RHC_time,SA_fitness,SA_time,GA_fitness,GA_time,MIMIC_fitness,MIMIC_time\n')

# The number of items
NUM_ITEMS = 100
# The number of copies each
COPIES_EACH = 4
# The maximum weight for a single element
MAX_WEIGHT = 50
# The maximum volume for a single element
MAX_VOLUME = 50
# The volume of the knapsack 
KNAPSACK_VOLUME = MAX_VOLUME * NUM_ITEMS * COPIES_EACH * .4

# create copies
fill = [COPIES_EACH] * NUM_ITEMS
copies = array('i', fill)

# create weights and volumes
fill = [0] * NUM_ITEMS
weights = array('d', fill)
volumes = array('d', fill)
for i in range(0, NUM_ITEMS):
    weights[i] = random.nextDouble() * MAX_WEIGHT
    volumes[i] = random.nextDouble() * MAX_VOLUME

# create range
fill = [COPIES_EACH + 1] * NUM_ITEMS
ranges = array('i', fill)


ef = KnapsackEvaluationFunction(weights, volumes, KNAPSACK_VOLUME, copies)
odd = DiscreteUniformDistribution(ranges)
nf = DiscreteChangeOneNeighbor(ranges)
mf = DiscreteChangeOneMutation(ranges)
cf = UniformCrossOver()
df = DiscreteDependencyTree(.1, ranges)
hcp = GenericHillClimbingProblem(ef, odd, nf)
gap = GenericGeneticAlgorithmProblem(ef, odd, mf, cf)
pop = GenericProbabilisticOptimizationProblem(ef, odd, df)


for iterations in range(100,2001,100) + range(2100,20001,1000):
    out.write(str(iterations) + ',')

    duration = 0
    fitness = 0
    for i in range(RUNS):
        rhc = RandomizedHillClimbing(hcp)
        fit = FixedIterationTrainer(rhc, iterations)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(rhc.getOptimal())
        print "RHC: " + str(ef.value(rhc.getOptimal()))
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

    duration = 0
    fitness = 0
    for i in range(RUNS):
        sa = SimulatedAnnealing(100, .95, hcp)
        fit = FixedIterationTrainer(sa, iterations)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(sa.getOptimal())
        print "SA: " + str(ef.value(sa.getOptimal()))
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

    if iterations <= 2000:
        duration = 0
        fitness = 0
        for i in range(RUNS):
            ga = StandardGeneticAlgorithm(200, 150, 25, gap)
            fit = FixedIterationTrainer(ga, iterations)
            st = time.time()
            fit.train()
            duration += time.time() - st
            fitness += ef.value(ga.getOptimal())
            print "GA: " + str(ef.value(ga.getOptimal()))
        out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

        duration = 0
        fitness = 0
        for i in range(RUNS):
            mimic = MIMIC(200, 100, pop)
            fit = FixedIterationTrainer(mimic, iterations)
            st = time.time()
            fit.train()
            duration += time.time() - st
            fitness += ef.value(mimic.getOptimal())
            print "MIMIC: " + str(ef.value(mimic.getOptimal()))
        out.write(str(fitness/RUNS) + ',' + str(duration/RUNS))

    else:
        out.write("n/a,n/a,n/a,n/a")

    out.write('\n')
out.close()
