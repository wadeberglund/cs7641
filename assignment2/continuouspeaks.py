"""
Based on the original code in this repository:
https://github.com/pushkar/ABAGAIL.git

continuous peaks algorithm implementation in jython
"""

import sys
import os
import time

import java.io.FileReader as FileReader
import java.io.File as File
import java.lang.String as String
import java.lang.StringBuffer as StringBuffer
import java.lang.Boolean as Boolean
import java.util.Random as Random

sys.path.append("./ABAGAIL/ABAGAIL.jar")

import dist.DiscreteDependencyTree as DiscreteDependencyTree
import dist.DiscreteUniformDistribution as DiscreteUniformDistribution
import dist.Distribution as Distribution
import opt.DiscreteChangeOneNeighbor as DiscreteChangeOneNeighbor
import opt.EvaluationFunction as EvaluationFunction
import opt.GenericHillClimbingProblem as GenericHillClimbingProblem
import opt.HillClimbingProblem as HillClimbingProblem
import opt.NeighborFunction as NeighborFunction
import opt.RandomizedHillClimbing as RandomizedHillClimbing
import opt.SimulatedAnnealing as SimulatedAnnealing
import opt.example.FourPeaksEvaluationFunction as FourPeaksEvaluationFunction
import opt.ga.CrossoverFunction as CrossoverFunction
import opt.ga.SingleCrossOver as SingleCrossOver
import opt.ga.DiscreteChangeOneMutation as DiscreteChangeOneMutation
import opt.ga.GenericGeneticAlgorithmProblem as GenericGeneticAlgorithmProblem
import opt.ga.GeneticAlgorithmProblem as GeneticAlgorithmProblem
import opt.ga.MutationFunction as MutationFunction
import opt.ga.StandardGeneticAlgorithm as StandardGeneticAlgorithm
import opt.ga.UniformCrossOver as UniformCrossOver
import opt.prob.GenericProbabilisticOptimizationProblem as GenericProbabilisticOptimizationProblem
import opt.prob.MIMIC as MIMIC
import opt.prob.ProbabilisticOptimizationProblem as ProbabilisticOptimizationProblem
import shared.FixedIterationTrainer as FixedIterationTrainer
import opt.example.ContinuousPeaksEvaluationFunction as ContinuousPeaksEvaluationFunction
from array import array



"""
Commandline parameter(s):
   none
"""

if not os.path.isdir('./ro_output'):
    os.mkdir('./ro_output')

RUNS = 10

# Sweep N and report fitness and time, averaged across 10 runs
outfile = './ro_output/continuouspeaks_N.csv'
out = open(outfile, 'w')
out.write('T,RHC_fitness,RHC_time,SA_fitness,SA_time,GA_fitness,GA_time,MIMIC_fitness,MIMIC_time\n')

N = 80

for T in [2,4,8,14,16,18,20]:
    out.write(str(T) + ',')

    fill = [2] * N
    ranges = array('i', fill)

    ef = ContinuousPeaksEvaluationFunction(T)
    odd = DiscreteUniformDistribution(ranges)
    nf = DiscreteChangeOneNeighbor(ranges)
    mf = DiscreteChangeOneMutation(ranges)
    cf = SingleCrossOver()
    df = DiscreteDependencyTree(.1, ranges)
    hcp = GenericHillClimbingProblem(ef, odd, nf)
    gap = GenericGeneticAlgorithmProblem(ef, odd, mf, cf)
    pop = GenericProbabilisticOptimizationProblem(ef, odd, df)

    duration = 0
    fitness = 0
    for i in range(RUNS):
        rhc = RandomizedHillClimbing(hcp)
        fit = FixedIterationTrainer(rhc, 200000)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(rhc.getOptimal())
        print "RHC: " + str(ef.value(rhc.getOptimal()))
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

    duration = 0
    fitness = 0
    for i in range(RUNS):
        sa = SimulatedAnnealing(1e11, 0.95, hcp)
        fit = FixedIterationTrainer(sa, 200000)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(sa.getOptimal())
        print "SA: " + str(ef.value(sa.getOptimal()))
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

    duration = 0
    fitness = 0
    for i in range(RUNS):
        ga = StandardGeneticAlgorithm(200, 100, 10, gap)
        fit = FixedIterationTrainer(ga, 1000)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(ga.getOptimal())
        print "GA: " + str(ef.value(ga.getOptimal()))
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

    duration = 0
    fitness = 0
    for i in range(RUNS):
        mimic = MIMIC(200, 20, pop)
        fit = FixedIterationTrainer(mimic, 100)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(mimic.getOptimal())
        print "MIMIC: " + str(ef.value(mimic.getOptimal()))
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS))

    out.write('\n')

out.close()

# Sweep number of iterations and report fitness, averaged across 10 runs
N=80
T=N/10
fill = [2] * N
ranges = array('i', fill)

ef = ContinuousPeaksEvaluationFunction(T)
odd = DiscreteUniformDistribution(ranges)
nf = DiscreteChangeOneNeighbor(ranges)
mf = DiscreteChangeOneMutation(ranges)
cf = SingleCrossOver()
df = DiscreteDependencyTree(.1, ranges)
hcp = GenericHillClimbingProblem(ef, odd, nf)
gap = GenericGeneticAlgorithmProblem(ef, odd, mf, cf)
pop = GenericProbabilisticOptimizationProblem(ef, odd, df)

outfile = './ro_output/continuouspeaks_iter.csv'
out = open(outfile, 'w')
out.write('Iteration,RHC_fitness,RHC_time,SA_fitness,SA_time,GA_fitness,GA_time,MIMIC_fitness,MIMIC_time\n')

for iterations in range(100,2001,100) + range(2100,100001,1000):
    out.write(str(iterations) + ',')

    duration = 0
    fitness = 0
    for i in range(RUNS):
        rhc = RandomizedHillClimbing(hcp)
        fit = FixedIterationTrainer(rhc, iterations)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(rhc.getOptimal())
    print "RHC: " + str(fitness/RUNS)
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

    duration = 0
    fitness = 0
    for i in range(RUNS):
        sa = SimulatedAnnealing(1e11, 0.95, hcp)
        fit = FixedIterationTrainer(sa, iterations)
        st = time.time()
        fit.train()
        duration += time.time() - st
        fitness += ef.value(sa.getOptimal())
    print "SA: " + str(fitness/RUNS)
    out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

    if iterations <= 2000:
        duration = 0
        fitness = 0
        for i in range(RUNS):
            ga = StandardGeneticAlgorithm(200, 100, 10, gap)
            fit = FixedIterationTrainer(ga, iterations)
            st = time.time()
            fit.train()
            duration += time.time() - st
            fitness += ef.value(ga.getOptimal())
        print "GA: " + str(fitness/RUNS)
        out.write(str(fitness/RUNS) + ',' + str(duration/RUNS) + ',')

        duration = 0
        fitness = 0
        for i in range(RUNS):
            mimic = MIMIC(200, 20, pop)
            fit = FixedIterationTrainer(mimic, iterations)
            st = time.time()
            fit.train()
            duration += time.time() - st
            fitness += ef.value(mimic.getOptimal())
        print "MIMIC: " + str(fitness/RUNS)
        out.write(str(fitness/RUNS) + ',' + str(duration/RUNS))

    else:
        out.write('n/a,n/a,n/a,n/a')

    out.write('\n')

out.close()
