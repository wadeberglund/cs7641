"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-2.git

Genetic Algorithm NN training
"""

import os
import csv
import time
import sys

sys.path.append("./ABAGAIL/ABAGAIL.jar")

from func.nn.backprop import BackPropagationNetworkFactory
from shared import SumOfSquaresError, DataSet, Instance
from opt.example import NeuralNetworkOptimizationProblem
from func.nn.backprop import RPROPUpdateRule, BatchBackPropagationTrainer
import opt.RandomizedHillClimbing as RandomizedHillClimbing
import opt.SimulatedAnnealing as SimulatedAnnealing
import opt.ga.StandardGeneticAlgorithm as StandardGeneticAlgorithm
from func.nn.activation import RELU

from helpers import initialize_instances, errorOnDataSet, train


# Network parameters found "optimal" in Assignment 1
INPUT_LAYER = 50
HIDDEN_LAYER1 = 50
HIDDEN_LAYER2 = 50
OUTPUT_LAYER = 1
TRAINING_ITERATIONS = 1001


def main(P, mate, mutate):
    """Run this experiment"""
    training   = initialize_instances("neutrino_trg.csv")
    testing    = initialize_instances("neutrino_tst.csv")
    validation = initialize_instances("neutrino_val.csv")

    factory = BackPropagationNetworkFactory()
    measure = SumOfSquaresError()
    data_set = DataSet(training)
    relu = RELU()
    rule = RPROPUpdateRule()

    outfile = "./nn_output/genetic_algorithm_{}_{}_{}.csv".format(P, mate, mutate)
    with open(outfile, 'w') as f:
        f.write('{},{},{},{},{},{},{},{}\n'.format('iteration',
                                                   'MSE_trg',
                                                   'MSE_val',
                                                   'MSE_tst',
                                                   'acc_trg',
                                                   'acc_val',
                                                   'acc_tst',
                                                   'elapsed'))

    classification_network = factory.createClassificationNetwork([INPUT_LAYER,
                                                                  HIDDEN_LAYER1,
                                                                  HIDDEN_LAYER2,
                                                                  OUTPUT_LAYER],
                                                                  relu)

    nnop = NeuralNetworkOptimizationProblem(data_set, classification_network, measure)
    oa = StandardGeneticAlgorithm(P, mate, mutate, nnop)
    train(oa, classification_network, outfile, training, validation, testing, measure, TRAINING_ITERATIONS)



if __name__ == "__main__":
    # P is population size, mate is the number to mate per iteration, mutate is the number to mutate per iteration
    for p in [50,100,200]:
        for mate in [10,20,40]:
            for mutate in [5,10,20]:
                nmate = int(p*mate/100.)
                nmutate = int(p*mutate/100.)
                main(p, nmate, nmutate)

