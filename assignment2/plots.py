#!/usr/bin/env python3

"""
Make plots using csv data generated during trial runs.
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def plot_nn(title, infile, outfile):

    nncsv = pd.read_csv(infile)
    xs = nncsv['iteration']

    # Plot
    plt.figure()
    plt.title(title)
    plt.xlabel("Iterations")
    plt.ylabel("Accuracy")

    # box-like grid
    plt.grid()

    # plot the average training and test score lines at each training set size
    plt.plot(xs, nncsv['acc_tst'], '-', color="k",  label="Test Accuracy")
    plt.plot(xs, nncsv['acc_val'], '--', color="k", label="Validation Accuracy")
    plt.plot(xs, nncsv['acc_trg'], ':', color="k",  label="Training Accuracy")
    plt.legend(loc="best")

    # sizes the window for readability and displays the plot
    plt.savefig(outfile, dpi=300, bbox_inches='tight')


def plot_nn_time(infile1, infile2, infile3):

    csv1 = pd.read_csv(infile1)
    csv2 = pd.read_csv(infile2)
    csv3 = pd.read_csv(infile3)

    # Plot
    plt.figure()
    plt.title("ANN Training Time")
    plt.xlabel("Iterations")
    plt.ylabel("Elapsed time (s)")

    # box-like grid
    plt.grid()

    # plot the average training and test score lines at each training set size
    plt.plot(csv1['iteration'], csv1['elapsed'], '-', color="k",  label="RHC time")
    plt.plot(csv2['iteration'], csv2['elapsed'], '--', color="k",  label="SA time")
    plt.plot(csv3['iteration'], csv3['elapsed'], ':', color="k",  label="GA time")
    plt.legend(loc="best")

    # sizes the window for readability and displays the plot
    outfile = 'nn_output/nn_time.tiff'
    plt.savefig(outfile, dpi=300, bbox_inches='tight')


def plot_ro_v_N(title, x, infile, outfile):

    nncsv = pd.read_csv(infile)
    xs = nncsv[x]

    # Plot
    plt.figure()
    plt.title(title + ' Fitness')
    plt.xlabel(x)
    plt.ylabel("Fitness")

    # box-like grid
    plt.grid()

    # plot the average training and test score lines at each training set size
    plt.plot(xs, nncsv['RHC_fitness'], '-', color="k", label="RHC")
    plt.plot(xs, nncsv['SA_fitness'], '--', color="k", label="SA")
    plt.plot(xs, nncsv['GA_fitness'], ':', color="k",  label="GA")
    plt.plot(xs, nncsv['MIMIC_fitness'], '-.', color="k", label="MIMIC")
    plt.legend(loc="best")

    # sizes the window for readability and displays the plot
    plt.savefig(outfile+'_N.tiff', dpi=300, bbox_inches='tight')

    # Plot
    plt.figure()
    plt.title(title + ' Fitness')
    plt.xlabel(x)
    plt.ylabel("Fitness")

    # box-like grid
    plt.grid()

    # plot the average training and test score lines at each training set size
    nncsv_short = nncsv[nncsv[x]<=2000]
    plt.plot(nncsv_short[x], nncsv_short['RHC_fitness'], '-', color="k", label="RHC")
    plt.plot(nncsv_short[x], nncsv_short['SA_fitness'], '--', color="k", label="SA")
    plt.plot(nncsv_short[x], nncsv_short['GA_fitness'], ':', color="k",  label="GA")
    plt.plot(nncsv_short[x], nncsv_short['MIMIC_fitness'], '-.', color="k", label="MIMIC")
    plt.legend(loc="best")

    # sizes the window for readability and displays the plot
    plt.savefig(outfile+'_short_N.tiff', dpi=300, bbox_inches='tight')

    # Plot
    plt.figure()
    plt.title(title + ' Runtime')
    plt.xlabel(x)
    plt.ylabel("Time (s)")
    plt.yscale("log")

    # box-like grid
    plt.grid()

    # plot the average training and test score lines at each training set size
    plt.plot(xs, nncsv['RHC_time'], '-', color="k", label="RHC")
    plt.plot(xs, nncsv['SA_time'], '--', color="k", label="SA")
    plt.plot(xs, nncsv['GA_time'], ':', color="k",  label="GA")
    plt.plot(xs, nncsv['MIMIC_time'], '-.', color="k", label="MIMIC")
    plt.legend(loc="best")

    # sizes the window for readability and displays the plot
    plt.savefig(outfile+'_time.tiff', dpi=300, bbox_inches='tight')


plot_nn('Random Hill Climbing Accuracy', 'nn_output/random_hill_climbing.csv', 'nn_output/random_hill_climbing.tiff')
plot_nn('Simulated Annealing Accuracy', 'nn_output/simulated_annealing_0.15.csv', 'nn_output/simulated_annealing.tiff')
plot_nn('Genetic Algorithm Accuracy', 'nn_output/genetic_algorithm_100_40_10.csv', 'nn_output/genetic_algorithm.tiff')

plot_nn_time('nn_output/random_hill_climbing.csv', 'nn_output/simulated_annealing_0.15.csv', 'nn_output/genetic_algorithm_100_40_10.csv')

plot_ro_v_N('Continuous Peaks', 'N', 'ro_output/continuouspeaks_N.csv', 'ro_output/continuouspeaks')
plot_ro_v_N('Continuous Peaks', 'Iteration', 'ro_output/continuouspeaks_iter.csv', 'ro_output/continuouspeaks_iter')

plot_ro_v_N('Travelling Salesman', 'N', 'ro_output/tsp_N.csv', 'ro_output/tsp')
plot_ro_v_N('Travelling Salesman', 'Iteration', 'ro_output/tsp_iter.csv', 'ro_output/tsp_iter')

plot_ro_v_N('Knapsack', 'NUM_ITEMS', 'ro_output/knapsack_N.csv', 'ro_output/knapsack')
plot_ro_v_N('Knapsack', 'Iteration', 'ro_output/knapsack_iter.csv', 'ro_output/knapsack_iter')

