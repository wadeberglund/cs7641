#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-1.git
"""

import sys

import numpy as np
import pandas as pd

import sklearn.model_selection as ms
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.kernel_approximation import RBFSampler
from sklearn.linear_model import SGDClassifier
from sklearn.svm import SVC,LinearSVC

from helpers import basicResults, write_cm, write_learning_curve, makeTimingCurve, iterationLC


################################################################################
# Load Data, makes splits, create pipelines, fit data

neutrino_raw = pd.read_csv('MiniBooNE_PID.txt', sep='\s+', header=None)

neutrino = neutrino_raw.sample(20000)
neutrinoX = neutrino.iloc[:,:50].values.astype(np.float64)
neutrinoY = neutrino.iloc[:,50].values.astype(np.int)

neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY = ms.train_test_split(neutrinoX,
                                                                                 neutrinoY,
                                                                                 test_size=0.2,
                                                                                 random_state=25,
                                                                                 stratify=neutrinoY)


connect4 = pd.read_csv('connect-4.data')
connect4X = pd.get_dummies(connect4.loc[:,'a1':'g6'], drop_first=True).values.astype(np.float64)
connect4['result'].replace('draw', 0, inplace=True)
connect4['result'].replace('win',  1, inplace=True)
connect4['result'].replace('loss', 2, inplace=True)
connect4Y = connect4['result'].values.astype(np.int)

connect4_trgX, connect4_tstX, connect4_trgY, connect4_tstY = ms.train_test_split(connect4X,
                                                                                 connect4Y,
                                                                                 test_size=0.2,
                                                                                 random_state=34,
                                                                                 stratify=connect4Y)





################################################################################
# Linear SVM

pipeN = Pipeline([('Scale', StandardScaler()),
                  ('SVM', SVC(kernel='linear',class_weight='balanced',random_state=35))])

a, b = -4, 1
Cs = np.logspace(a,b,b-a+1)
param_gridN = {'SVM__C': Cs}

# neutrino_clf = basicResults(pipeN,
#                             neutrino_trgX,
#                             neutrino_trgY,
#                             neutrino_tstX,
#                             neutrino_tstY,
#                             param_gridN,
#                             'SVM_Lin',
#                             'neutrino')
# write_learning_curve(neutrino_clf.best_estimator_,
#                      neutrino_trgX,
#                      neutrino_trgY,
#                      'SVM_Lin',
#                      'neutrino')


pipeC = Pipeline([#('Scale',StandardScaler()),
                  ('SVM',LinearSVC(class_weight='balanced', random_state=36))])

# N_connect4 = connect4_trgX.shape[0]
# params_connect4 = {'SVM__alpha':alphas,
                   # 'SVM__max_iter':[int((1e6/N_connect4)/.8)+1]}

a, b = -4, 2
Cs = np.logspace(a,b,b-a+1)
param_gridC = {'SVM__C': Cs}

# connect4_clf = basicResults(pipeC,
#                             connect4_trgX,
#                             connect4_trgY,
#                             connect4_tstX,
#                             connect4_tstY,
#                             param_gridC,
#                             'SVM_Lin',
#                             'connect4')
# write_learning_curve(connect4_clf.best_estimator_,
#                      connect4_trgX,
#                      connect4_trgY,
#                      'SVM_Lin',
#                      'connect4')


################################################################################
# Set final parameters and summarize data

# neutrino_final_params = neutrino_clf.best_params_
neutrino_final_params = {'SVM__C': 10.0}
pipeC.set_params(**neutrino_final_params)


# Classification report and confusion matrix
write_cm(pipeN,
         neutrino_trgX,
         neutrino_trgY,
         neutrino_tstX,
         neutrino_tstY,
         'SVM_Lin',
         'neutrino')

# Learning curve
write_learning_curve(pipeN,
                     neutrino_trgX,
                     neutrino_trgY,
                     'SVM_Lin',
                     'neutrino')

# Timing statistics
makeTimingCurve(pipeN,
                neutrinoX,
                neutrinoY,
                'SVM_Lin',
                'neutrino')

# Regularization tuning. C=100 has very low bias (will overfit) while larger values, especially positive ones, will
# fail to fit well.
a, b = -4, 2
Cs = np.logspace(a,b,b-a+1)
iterationLC(pipeN,
            neutrino_trgX,
            neutrino_trgY,
            neutrino_tstX,
            neutrino_tstY,
            {'SVM__C': Cs},
            'SVM_Lin',
            'neutrino',
            grid_search=False,
            xscale='log')

# Performance when tuning the max number of iterations
iterationLC(pipeN,
            neutrino_trgX,
            neutrino_trgY,
            neutrino_tstX,
            neutrino_tstY,
            {'SVM__max_iter':[2**x for x in range(12)]},
            'SVM_Lin',
            'neutrino',
            grid_search=False)


# connect4_final_params =connect4_clf.best_params_
connect4_final_params ={'SVM__C': 1.0}
pipeC.set_params(**connect4_final_params)

# Classification report and confusion matrix
write_cm(pipeC,
         connect4_trgX,
         connect4_trgY,
         connect4_tstX,
         connect4_tstY,
         'SVM_Lin',
         'connect4')

# Learning curve
write_learning_curve(pipeC,
                     connect4_trgX,
                     connect4_trgY,
                     'SVM_Lin',
                     'connect4')

# Timing statistics
makeTimingCurve(pipeC,
                connect4X,
                connect4Y,
                'SVM_Lin',
                'connect4')

# Regularization tuning. C=100 has very low bias (will overfit) while larger values, especially positive ones, will
# fail to fit well.
a, b = -4, 2
Cs = np.logspace(a,b,b-a+1)
iterationLC(pipeC,
            connect4_trgX,
            connect4_trgY,
            connect4_tstX,
            connect4_tstY,
            {'SVM__C': Cs},
            'SVM_Lin',
            'connect4',
            grid_search=False,
            xscale='log')

# Performance when tuning the max number of iterations
iterationLC(pipeC,
            connect4_trgX,
            connect4_trgY,
            connect4_tstX,
            connect4_tstY,
            {'SVM__max_iter':[2**x for x in range(12)]},
            'SVM_Lin',
            'connect4',
            grid_search=False)


