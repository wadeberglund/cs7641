#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-1.git
"""

import sys

import numpy as np
import pandas as pd

import sklearn.model_selection as ms
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.kernel_approximation import RBFSampler,Nystroem
from sklearn.linear_model import SGDClassifier
from sklearn.svm import SVC,LinearSVC

from helpers import basicResults, write_cm, write_learning_curve, makeTimingCurve, iterationLC


################################################################################
# Load Data, makes splits, create pipelines, fit data

neutrino_raw = pd.read_csv('MiniBooNE_PID.txt', sep='\s+', header=None)

neutrino = neutrino_raw.sample(20000)
neutrinoX = neutrino.iloc[:,:50].values.astype(np.float64)
neutrinoY = neutrino.iloc[:,50].values.astype(np.int)

neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY = ms.train_test_split(neutrinoX,
                                                                                 neutrinoY,
                                                                                 test_size=0.2,
                                                                                 random_state=25,
                                                                                 stratify=neutrinoY)


connect4 = pd.read_csv('connect-4.data')
connect4X = pd.get_dummies(connect4.loc[:,'a1':'g6'], drop_first=True).values.astype(np.float64)
connect4['result'].replace('draw', 0, inplace=True)
connect4['result'].replace('win',  1, inplace=True)
connect4['result'].replace('loss', 2, inplace=True)
connect4Y = connect4['result'].values.astype(np.int)

connect4_trgX, connect4_tstX, connect4_trgY, connect4_tstY = ms.train_test_split(connect4X,
                                                                                 connect4Y,
                                                                                 test_size=0.2,
                                                                                 random_state=38,
                                                                                 stratify=connect4Y)



################################################################################
# RBF SVM

pipeN = Pipeline([('Scale',StandardScaler()),
                  ('SVM', SVC(tol=1e-3,
                              class_weight='balanced',
                              cache_size=2000,
                              random_state=39))])

# C controls overfitting in an SVM
a, b = -4, 2
gammas = np.logspace(a,b,b-a+1)
a, b = -2, 3
Cs = np.logspace(a,b,b-a+1)

param_gridN = {'SVM__C': Cs,
               'SVM__gamma': gammas,
               }

# neutrino_clf = basicResults(pipeN,
#                             neutrino_trgX,
#                             neutrino_trgY,
#                             neutrino_tstX,
#                             neutrino_tstY,
#                             param_gridN,
#                             'SVM_RBF',
#                             'neutrino')
# write_learning_curve(neutrino_clf.best_estimator_,
#                      neutrino_trgX,
#                      neutrino_trgY,
#                      'SVM_RBF',
#                      'neutrino')


a, b = -4, 2
gammas = np.logspace(a,b,b-a+1)
a, b = -4, 2
Cs = np.logspace(a,b,b-a+1)
alphas = [10**-x for x in np.arange(3,7.01,0.5)]

# The RBF sampler estimates the kernel functino by doing a MC sample of the Fourier spectrum of the data. It is an
# approximation, but may be close enough to the real kernel for very large data sets. See how big n_components needs to
# be to converge to the real RBFSVM.
pipeC = Pipeline([#('Scale',StandardScaler()),
                  # ('Sampler', RBFSampler(n_components=1000,
                  #                        random_state=40)),
                  ('Sampler', Nystroem(n_components=1000,
                                       random_state=40)),
                  ('SVM', SGDClassifier(max_iter=None,
                                        tol=1e-3,
                                        class_weight='balanced',
                                        random_state=41))])
param_gridC = {'Sampler__gamma': gammas,
               'SVM__alpha': alphas,
               'SVM__penalty': ['l2', 'l1', 'elasticnet'],
               'SVM__l1_ratio': [0,0.25,0.5,0.75,1],
               }


# Regular SVM pipleline
# pipeC = Pipeline([('Scale',StandardScaler()),
#                   ('SVM', SVC(tol=1e-5,
#                               class_weight='balanced',
#                               cache_size=2000,
#                               random_state=41))])
# param_gridC = {'SVM__C': Cs,
#                'SVM__gamma': gammas,
#                }


# connect4_clf = basicResults(pipeC,
#                             connect4_trgX,
#                             connect4_trgY,
#                             connect4_tstX,
#                             connect4_tstY,
#                             param_gridC,
#                             'SVM_RBF',
#                             'connect4')
# write_learning_curve(connect4_clf.best_estimator_,
#                      connect4_trgX,
#                      connect4_trgY,
#                      'SVM_RBF',
#                      'connect4')



################################################################################
# Set final parameters and summarize data

# neutrino_final_params = neutrino_clf.best_params_
neutrino_final_params = {'SVM__C': 10.0,
                         'SVM__gamma': 0.01}
pipeN.set_params(**neutrino_final_params)


# Classification report and confusion matrix
write_cm(pipeN,
         neutrino_trgX,
         neutrino_trgY,
         neutrino_tstX,
         neutrino_tstY,
         'SVM_RBF',
         'neutrino')

# Learning curve
write_learning_curve(pipeN,
                     neutrino_trgX,
                     neutrino_trgY,
                     'SVM_RBF',
                     'neutrino')

# Timing statistics
makeTimingCurve(pipeN,
                neutrinoX,
                neutrinoY,
                'SVM_RBF',
                'neutrino')

# Regularization tuning. C=100 has very low bias (will overfit) while larger values, especially positive ones, will
# fail to fit well.
a, b = -4, 2
Cs = np.logspace(a,b,b-a+1)
iterationLC(pipeN,
            neutrino_trgX,
            neutrino_trgY,
            neutrino_tstX,
            neutrino_tstY,
            {'SVM__C': Cs},
            'SVM_RBF',
            'neutrino',
            grid_search=False,
            xscale='log')

# Performance when tuning the total number of iterations
iterationLC(pipeN,
            neutrino_trgX,
            neutrino_trgY,
            neutrino_tstX,
            neutrino_tstY,
            {'SVM__max_iter':[2**x for x in range(12)]},
            'SVM_RBF',
            'neutrino',
            grid_search=False)


# connect4_final_params = connect4_clf.best_params_
connect4_final_params = {'Sampler__gamma': 0.1,
                         'SVM__alpha': 1e-6,
                         'SVM__penalty': 'elasticnet',
                         'SVM__l1_ratio': 0.5}
pipeC.set_params(**connect4_final_params)


# Classification report and confusion matrix
write_cm(pipeC,
         connect4_trgX,
         connect4_trgY,
         connect4_tstX,
         connect4_tstY,
         'SVM_RBF',
         'connect4')

# Learning curve
write_learning_curve(pipeC,
                     connect4_trgX,
                     connect4_trgY,
                     'SVM_RBF',
                     'connect4')

# Timing statistics
makeTimingCurve(pipeC,
                connect4X,
                connect4Y,
                'SVM_RBF',
                'connect4')

# Regularization tuning. alpha=0 has very low bias (will overfit) while larger values, especially positive ones, will
# fail to fit well.
alphas = [10**-x for x in np.arange(3,7.01,0.5)]
iterationLC(pipeC,
            connect4_trgX,
            connect4_trgY,
            connect4_tstX,
            connect4_tstY,
            {'SVM__alpha': alphas},
            'SVM_RBF',
            'connect4',
            grid_search=False,
            xscale='log')

# Performance when tuning the total number of iterations
iterationLC(pipeC,
            connect4_trgX,
            connect4_trgY,
            connect4_tstX,
            connect4_tstY,
            {'SVM__max_iter':[2**x for x in range(12)]},
            'SVM_RBF',
            'connect4',
            grid_search=False)

