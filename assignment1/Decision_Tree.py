#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-1.git
"""

import sys

import numpy as np
import pandas as pd
import sklearn

import sklearn.model_selection as ms
from sklearn.utils.random import sample_without_replacement
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler,OneHotEncoder
from sklearn.tree import DecisionTreeClassifier

from helpers import dtclf_pruned
from helpers import basicResults, write_cm, write_learning_curve, makeTimingCurve, iterationLC


def DTpruningVSnodes(clf,alphas,trgX,trgY,dataset):
    '''Dump table of pruning alpha vs. # of internal nodes'''
    out = {}
    for a in alphas:
        clf.set_params(**{'DT__alpha': a})
        clf.fit(trgX,trgY)
        out[a]=clf.steps[-1][-1].numNodes()
        print(dataset,a)
    out = pd.Series(out)
    out.index.name='alpha'
    out.name = 'Number of Internal Nodes'
    out.to_csv('./output/DT_{}_nodecounts.csv'.format(dataset))


################################################################################
# Load Data, makes splits, create pipelines, fit data

neutrino_raw = pd.read_csv('MiniBooNE_PID.txt', sep='\s+', header=None)

neutrino = neutrino_raw.sample(20000)
neutrinoX = neutrino.iloc[:,:50].values.astype(np.float64)
neutrinoY = neutrino.iloc[:,50].values.astype(np.int)

neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY = ms.train_test_split(neutrinoX,
                                                                                 neutrinoY,
                                                                                 test_size=0.2,
                                                                                 random_state=16,
                                                                                 stratify=neutrinoY)

pipeN = Pipeline([('Scale', StandardScaler()),
                  ('DT', dtclf_pruned(random_state=17))])
                  # ('DT', DecisionTreeClassifier(random_state=17))])



connect4 = pd.read_csv('connect-4.data')
connect4['result'].replace('draw', 0, inplace=True)
connect4['result'].replace('win',  1, inplace=True)
connect4['result'].replace('loss', 2, inplace=True)

connect4X = pd.get_dummies(connect4.loc[:,'a1':'g6'], drop_first=True).values.astype(np.float64)
# connect4X = pd.get_dummies(connect4.iloc[:,[6,7,12,13,14,18,19,20,21,24,25,26,30,31]], drop_first=True).values.astype(np.float64)
connect4Y = connect4['result'].values.astype(np.int)

connect4_trgX, connect4_tstX, connect4_trgY, connect4_tstY = ms.train_test_split(connect4X,
                                                                                 connect4Y,
                                                                                 test_size=0.2,
                                                                                 random_state=17,
                                                                                 stratify=connect4Y)


pipeC = Pipeline([#('Scale',StandardScaler()),
                  ('DT',dtclf_pruned(random_state=18))])
                  # ('DT', DecisionTreeClassifier(random_state=18))])


# alpha determines how much pruning is allowed to be done until the results are no longer acceptible and controls
# overfitting
# alphas = [-1e-1,
#           -(1e-1)*10**-0.5,
#           -1e-2,
#           -(1e-2)*10**-0.5,
#           -1e-3,
#           -(1e-3)*10**-0.5,
#           0,
#           (1e-3)*10**-0.5,
#           1e-3,
#           (1e-2)*10**-0.5,
#           1e-2,
#           (1e-1)*10**-0.5,
#           1e-1,
#           ]

alphas = [-1,
          -1e-1,
          -(1e-1)*10**-0.5,
          -1e-2,
          -(1e-2)*10**-0.5,
          -1e-3,
          -(1e-3)*10**-0.5,
          0,
          (1e-3)*10**-0.5,
          1e-3,
          (1e-2)*10**-0.5,
          1e-2,
          (1e-1)*10**-0.5,
          1e-1,
          ]

param_grid = {'DT__criterion': ['gini','entropy'],
              'DT__alpha': alphas,
              'DT__class_weight': ['balanced',None]}

# neutrino_clf = basicResults(pipeN,
#                             neutrino_trgX,
#                             neutrino_trgY,
#                             neutrino_tstX,
#                             neutrino_tstY,
#                             param_grid,
#                             'DT',
#                             'neutrino')
# write_learning_curve(neutrino_clf.best_estimator_,
#                      neutrino_trgX,
#                      neutrino_trgY,
#                      'DT',
#                      'neutrino')


# connect4_clf = basicResults(pipeC,
#                             connect4_trgX,
#                             connect4_trgY,
#                             connect4_tstX,
#                             connect4_tstY,
#                             param_grid,
#                             'DT',
#                             'connect4')
# write_learning_curve(connect4_clf.best_estimator_,
#                      connect4_trgX,
#                      connect4_trgY,
#                      'DT',
#                      'connect4')


################################################################################
# Set final parameters and summarize data

# neutrino_final_params = neutrino_clf.best_params_
neutrino_final_params = {'DT__criterion': 'entropy',
                         'DT__alpha': 0,
                         'DT__class_weight': 'balanced'}
pipeN.set_params(**neutrino_final_params)


# Classification report and confusion matrix
write_cm(pipeN,
         neutrino_trgX,
         neutrino_trgY,
         neutrino_tstX,
         neutrino_tstY,
         'DT',
         'neutrino')

# Learning curve
write_learning_curve(pipeN,
                     neutrino_trgX,
                     neutrino_trgY,
                     'DT',
                     'neutrino')

# Timing statistics
makeTimingCurve(pipeN,
                neutrinoX,
                neutrinoY,
                'DT',
                'neutrino')

# Report tree pruning statistics
DTpruningVSnodes(pipeN,
                 alphas,
                 neutrino_trgX,
                 neutrino_trgY,
                 'neutrino')

# Regularization tuning. alpha=-1 has very low bias (will overfit) while larger values, especially positive ones, will
# fail to fit well.
iterationLC(pipeN,
            neutrino_trgX,
            neutrino_trgY,
            neutrino_tstX,
            neutrino_tstY,
            {'DT__alpha': alphas},
            'DT',
            'neutrino',
            grid_search=False,
            xscale='linear')


# connect4_final_params = connect4_clf.best_params_
connect4_final_params = {'DT__criterion': 'entropy',
                         'DT__alpha': 0,
                         'DT__class_weight': 'balanced'}
pipeC.set_params(**connect4_final_params)


# Classification report and confusion matrix
write_cm(pipeC,
         connect4_trgX,
         connect4_trgY,
         connect4_tstX,
         connect4_tstY,
         'DT',
         'connect4')

# Learning curve
write_learning_curve(pipeC,
                     connect4_trgX,
                     connect4_trgY,
                     'DT',
                     'connect4')

# Timing statistics
makeTimingCurve(pipeC,
                connect4X,
                connect4Y,
                'DT',
                'connect4')

# Report tree pruning statistics
DTpruningVSnodes(pipeC,
                 alphas,
                 connect4_trgX,
                 connect4_trgY,
                 'connect4')

# Regularization tuning. alpha=-1 has very low bias (will overfit) while larger values, especially positive ones, will
# fail to fit well.
iterationLC(pipeC,
            connect4_trgX,
            connect4_trgY,
            connect4_tstX,
            connect4_tstY,
            {'DT__alpha': alphas},
            'DT',
            'connect4',
            grid_search=False,
            xscale='linear')
