#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-1.git
"""

import sys

import numpy as np
import pandas as pd

import sklearn.model_selection as ms
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

from helpers import basicResults, write_cm, write_learning_curve, makeTimingCurve, iterationLC


################################################################################
# Load Data, makes splits, create pipelines, fit data

neutrino_raw = pd.read_csv('MiniBooNE_PID.txt', sep='\s+', header=None)

neutrino = neutrino_raw.sample(20000)
neutrinoX = neutrino.iloc[:,:50].values.astype(np.float64)
neutrinoY = neutrino.iloc[:,50].values.astype(np.int)

neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY = ms.train_test_split(neutrinoX,
                                                                                 neutrinoY,
                                                                                 test_size=0.2,
                                                                                 random_state=25,
                                                                                 stratify=neutrinoY)

pipeN = Pipeline([('Scale',StandardScaler()),
                  ('MLP',MLPClassifier(max_iter=2000,tol=1e-5,early_stopping=True,random_state=30))])
                  # ('MLP',MLPClassifier(max_iter=2000,early_stopping=True,random_state=30))])

# alpha is the L2 penalty, regularization term, defaults to 1e-4
# sgd solver allows you to tweak more parameters, possibly explore this
d = neutrinoX.shape[1]
alphas = [10**-x for x in np.arange(-1,5.01,0.5)] # 10, 3, 1, ..., 1e-5
hiddens = [(h,)*l for l in [1,2,3] for h in [d//2,d,2*d,3*d]]

param_gridN = {'MLP__activation': ['relu','logistic','tanh'],
               'MLP__alpha': alphas,
               'MLP__hidden_layer_sizes': hiddens}

# neutrino_clf = basicResults(pipeN,
#                             neutrino_trgX,
#                             neutrino_trgY,
#                             neutrino_tstX,
#                             neutrino_tstY,
#                             param_gridN,
#                             'ANN',
#                             'neutrino')
# write_learning_curve(neutrino_clf.best_estimator_,
#                      neutrino_trgX,
#                      neutrino_trgY,
#                      'ANN',
#                      'neutrino')


connect4 = pd.read_csv('connect-4.data')
connect4X = pd.get_dummies(connect4.loc[:,'a1':'g6'], drop_first=True).values.astype(np.float64)
connect4['result'].replace('draw', 0, inplace=True)
connect4['result'].replace('win',  1, inplace=True)
connect4['result'].replace('loss', 2, inplace=True)
connect4Y = connect4['result'].values.astype(np.int)


connect4_trgX, connect4_tstX, connect4_trgY, connect4_tstY = ms.train_test_split(connect4X,
                                                                                 connect4Y,
                                                                                 test_size=0.2,
                                                                                 random_state=31,
                                                                                 stratify=connect4Y)


pipeC = Pipeline([#('Scale',StandardScaler()),
                  ('MLP',MLPClassifier(max_iter=2000,early_stopping=True,random_state=32))])

d = connect4X.shape[1]
alphas = [10**-x for x in np.arange(0,5.01,1/2)] # 10, 3, 1, ..., 1e-5
hiddens = [(h,)*l for l in [1,2,3] for h in [d,d//2,2*d]]
# learning_rate doesn't matter
# learning_rate=['constant','invscaling','adaptive']

param_gridC = {'MLP__activation': ['relu','logistic'],
               'MLP__alpha': alphas,
               # 'MLP__learning_rate': learning_rate,
               'MLP__hidden_layer_sizes': hiddens}

# connect4_clf = basicResults(pipeC,
#                             connect4_trgX,
#                             connect4_trgY,
#                             connect4_tstX,
#                             connect4_tstY,
#                             param_gridC,
#                             'ANN',
#                             'connect4')
# write_learning_curve(connect4_clf.best_estimator_,
#                      connect4_trgX,
#                      connect4_trgY,
#                      'ANN',
#                      'connect4')


################################################################################
# Set final parameters and summarize data

# neutrino_final_params = neutrino_clf.best_params_
neutrino_final_params = {'MLP__activation': 'tanh',
                         'MLP__alpha': 1e-4,
                         'MLP__hidden_layer_sizes': (150,150)}
pipeN.set_params(**neutrino_final_params)


# Classification report and confusion matrix
write_cm(pipeN,
         neutrino_trgX,
         neutrino_trgY,
         neutrino_tstX,
         neutrino_tstY,
         'ANN',
         'neutrino')

# Learning curve
write_learning_curve(pipeN,
                     neutrino_trgX,
                     neutrino_trgY,
                     'ANN',
                     'neutrino')

# Timing statistics
makeTimingCurve(pipeN,
                neutrinoX,
                neutrinoY,
                'ANN',
                'neutrino')

# Regularization tuning. alpha=0 has very low bias (will overfit) while larger values, especially positive ones, will
# fail to fit well.
alphas = [0] + [10**-x for x in np.arange(6,0,-0.5)]
iterationLC(pipeN,
            neutrino_trgX,
            neutrino_trgY,
            neutrino_tstX,
            neutrino_tstY,
            {'MLP__alpha': alphas},
            'ANN',
            'neutrino',
            grid_search=False,
            xscale='log')

# Performance when tuning the total number of iterations
pipeN.set_params(**{'MLP__early_stopping':False})
iterationLC(pipeN,
            neutrino_trgX,
            neutrino_trgY,
            neutrino_tstX,
            neutrino_tstY,
            {'MLP__max_iter':[2**x for x in range(9)]},
            'ANN',
            'neutrino',
            grid_search=False)


# connect4_final_params = connect4_clf.best_params_
connect4_final_params = {'MLP__activation': 'relu',
                         'MLP__alpha': 3e-4,
                         'MLP__hidden_layer_sizes': (220,220,220)}
pipeC.set_params(**connect4_final_params)


# Classification report and confusion matrix
write_cm(pipeC,
         connect4_trgX,
         connect4_trgY,
         connect4_tstX,
         connect4_tstY,
         'ANN',
         'connect4')

# Learning curve
write_learning_curve(pipeC,
                     connect4_trgX,
                     connect4_trgY,
                     'ANN',
                     'connect4')

# Timing statistics
makeTimingCurve(pipeC,
                connect4X,
                connect4Y,
                'ANN',
                'connect4')

# Regularization tuning. alpha=0 has very low bias (will overfit) while larger values, especially positive ones, will
# fail to fit well.
alphas = [0] + [10**-x for x in np.arange(6,0,-0.5)]
iterationLC(pipeC,
            connect4_trgX,
            connect4_trgY,
            connect4_tstX,
            connect4_tstY,
            {'MLP__alpha': alphas},
            'ANN',
            'connect4',
            grid_search=False,
            xscale='log')


# Performance when tuning the total number of iterations
pipeC.set_params(**{'MLP__early_stopping':False})
iterationLC(pipeC,
            connect4_trgX,
            connect4_trgY,
            connect4_tstX,
            connect4_tstY,
            {'MLP__max_iter':[2**x for x in range(12)]},
            'ANN',
            'connect4',
            grid_search=False)


