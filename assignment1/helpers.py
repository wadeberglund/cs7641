#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-1.git
"""

import os
from time import process_time
from collections import defaultdict

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import sklearn.model_selection as ms
from sklearn.metrics import make_scorer,accuracy_score,classification_report,confusion_matrix
from sklearn.utils import compute_sample_weight
from sklearn.tree import DecisionTreeClassifier as dtclf


# Create our output directory if it does not already exist
if not os.path.isdir('./output'):
    os.mkdir('./output')


np.random.seed(10)

def balanced_accuracy(truth,pred):
    wts = compute_sample_weight('balanced',truth)
    return accuracy_score(truth,pred,sample_weight=wts)

scorer = make_scorer(balanced_accuracy)

def basicResults(clfObj,trgX,trgY,tstX,tstY,params,clf_type,dataset):
    '''
    Do a CV grid search across params to find the best classifier and then save off summarizing data.
    '''

    # Find the best classifier by searching across params
    print("GridSearchCV")
    cv = ms.GridSearchCV(clfObj,
                         n_jobs=4,
                         param_grid=params,
                         refit=True,
                         verbose=10,
                         cv=5,
                         scoring=scorer,
                         error_score=np.nan,
                         return_train_score=True)
    cv.fit(trgX,trgY)

    # Save off summary of results from GridSearchCV
    regTable = pd.DataFrame(cv.cv_results_)
    regTable.to_csv('./output/{}_{}_reg.csv'.format(clf_type,dataset),index=False)

    # Save off test scores and the parameters of the best classifier
    test_score = cv.score(tstX,tstY)
    with open('./output/test_results.csv','a') as f:
        f.write('{},{},{},{}\n'.format(clf_type,dataset,test_score,cv.best_params_))

    return cv


def write_cm(clf,trgX,trgY,tstX,tstY,clf_type,dataset):
    '''
    Write classification report and confusion matrix.
    '''

    clf.fit(trgX, trgY)
    predY = clf.predict(tstX)

    outfile = './output/{}_{}_CM.txt'.format(clf_type,dataset)
    with open(outfile, 'w') as f:
        score = balanced_accuracy(tstY,predY)
        print('Accuracy: ', score)
        f.write('Accuracy: {}'.format(score))

        cr = classification_report(tstY,predY)
        print(cr)
        f.write(cr)

        cm_df = pd.DataFrame(confusion_matrix(tstY, predY))
        print(cm_df.to_string())
        f.write(cm_df.to_string())


def write_learning_curve(clf,X,Y,clf_type,dataset):
    '''
    CV training and test scores for different training set sizes, the columns are the different CV sets. These are the
    learning curves.
    '''

    print("learning_curve")
    N = Y.shape[0]
    sizes, train_scores, test_scores = ms.learning_curve(clf,
                                    X,
                                    Y,
                                    n_jobs=4,
                                    cv=5,
                                    train_sizes=[16,100]+[(N*x//10)-1 for x in range(1,9)], # 10%, ..., 80%
                                    verbose=10,
                                    error_score=np.nan,
                                    scoring=scorer)

    curve_train_scores = pd.DataFrame(index=sizes, data=train_scores)
    curve_test_scores  = pd.DataFrame(index=sizes, data=test_scores)

    curve_train_scores.to_csv('./output/{}_{}_LC_train.csv'.format(clf_type,dataset))
    curve_test_scores.to_csv('./output/{}_{}_LC_test.csv'.format(clf_type,dataset))

    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    # Plot
    plt.figure()
    plt.title(dataset + " " + clf_type + " Learning Curves")
    plt.xlabel("training samples")
    plt.ylabel("accuracy (%)")
    # plt.gca().invert_yaxis()

    # box-like grid
    plt.grid()

    # plot the std deviation as a transparent range at each training set size
    plt.fill_between(sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.1, color="k")
    plt.fill_between(sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.1, color="k")

    # plot the average training and test score lines at each training set size
    plt.plot(sizes, train_scores_mean, '-', color="k", label="Training")
    plt.plot(sizes, test_scores_mean, '--', color="k", label="Cross-validation")
    # plt.legend(loc="best")
    plt.legend(loc="lower right")

    # sizes the window for readability and displays the plot
    # shows error from 0 to 1.1
    plt.ylim(-.1,1.1)
    outfile = './output/{}_{}_LC.tiff'.format(clf_type,dataset)
    plt.savefig(outfile, dpi=300, bbox_inches='tight')


def iterationLC(clfObj,trgX,trgY,tstX,tstY,params,clf_type,dataset,grid_search=True,xscale='linear'):
    '''
    Do a CV grid search across params to find the best classifier. Despite its name, this doesn't do learning curves,
    it just sweeps one parameter and reports accuracy at each step.
    '''

    # Find the best classifier by searching across params, in this case it should only be one parameter
    if grid_search:
        print("GridSearchCV")
        cv = ms.GridSearchCV(clfObj,
                            n_jobs=4,
                            param_grid=params,
                            refit=True,
                            verbose=10,
                            cv=5,
                            scoring=scorer,
                            return_train_score=True)
        cv.fit(trgX,trgY)

        # Save off summary of results from GridSearchCV
        regTable = pd.DataFrame(cv.cv_results_)
        regTable.to_csv('./output/{}_{}_single_sweep_reg.csv'.format(clf_type,dataset),index=False)

    # Save off the training/test accuracy across params showing how it converges
    d = defaultdict(list)
    name = list(params.keys())[0]
    sweep = name[name.index("__")+2:]
    print("Sweep {}".format(name))
    for value in list(params.values())[0]:
        print(value)
        d['param_{}'.format(name)].append(value)
        clfObj.set_params(**{name:value})

        clfObj.fit(trgX,trgY)
        pred = clfObj.predict(trgX)
        d['train accuracy'].append(balanced_accuracy(trgY,pred))

        pred = clfObj.predict(tstX)
        d['test accuracy'].append(balanced_accuracy(tstY,pred))


    d = pd.DataFrame(d)
    d.to_csv('./output/{}_{}_{}_sweep.csv'.format(clf_type,dataset,sweep),index=False)

    # Plot
    plt.figure()
    plt.title(dataset + " " + clf_type + " " + sweep + " Sweep")
    plt.xlabel(sweep)
    plt.ylabel("accuracy (%)")
    plt.xscale(xscale)
    # plt.gca().invert_yaxis()

    # box-like grid
    plt.grid()

    # plot the average training and test score lines at each training set size
    ys = d['param_{}'.format(name)]
    print(d)
    plt.plot(ys, d['train accuracy'], '-', color="k", label="Training")
    plt.plot(ys, d['test accuracy'], '--', color="k", label="Test")
    # plt.legend(loc="best")
    plt.legend(loc="lower right")

    # sizes the window for readability and displays the plot
    # shows error from 0 to 1.1
    plt.ylim(-.1,1.1)
    outfile = './output/{}_{}_{}_sweep.tiff'.format(clf_type,dataset,sweep)
    plt.savefig(outfile, dpi=300, bbox_inches='tight')


def add_noise(y,frac=0.1):
    n = y.shape[0]
    sz = int(n*frac)
    ind = np.random.choice(np.arange(n),size=sz,replace=False)
    tmp = y.copy()
    tmp[ind] = 1-tmp[ind]
    return tmp


def makeTimingCurve(clf,X,Y,clfName,dataset):
    '''
    Save off running time statistics for different train/test splits.
    '''

    print('makeTimingCurve')
    out = defaultdict(dict)
    fracs = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
    for frac in fracs:
        X_train, X_test, y_train, y_test = ms.train_test_split(X, Y, test_size=1-frac, random_state=13)

        # Time to fit the data
        st = process_time()
        clf.fit(X_train,y_train)
        out['train'][frac] = process_time()-st

        # Time to predict the data
        st = process_time()
        clf.predict(X_test)
        out['test'][frac] = process_time()-st

        print(clfName,dataset,frac)

    out = pd.DataFrame(out)
    out.to_csv('./output/{}_{}_timing.csv'.format(clfName,dataset))

    # Plot
    plt.figure()
    # _, ax1 = plt.subplots()
    plt.title(dataset + " " + clfName + " Runtime")
    plt.xlabel("training samples (%)")
    plt.ylabel("time (s)")
    # ax2 = ax1.twinx()
    # ax2.set_ylabel("time (ms)")

    # box-like grid
    plt.grid()

    # plot the average training and test score lines at each training set size
    plt.plot(fracs, out['train'], '-', color="k", label="Fit")
    plt.plot(fracs, out['test'], '--', color="k", label="Predict")
    plt.legend(loc="best")

    # sizes the window for readability and displays the plot
    # shows error from 0 to 1.1
    # plt.ylim(-.1,1.1)
    outfile = './output/{}_{}_timing.tiff'.format(clfName,dataset)
    plt.savefig(outfile, dpi=300, bbox_inches='tight')


def plot_all_timing_curves(dataset):
    files = [('DT', './output/DT_{}_timing.csv'.format(dataset)),
             ('Boost', './output/Boost_{}_timing.csv'.format(dataset)),
             ('KNN', './output/KNN_{}_timing.csv'.format(dataset)),
             ('ANN', './output/ANN_{}_timing.csv'.format(dataset)),
             ('SVM_Lin', './output/SVM_Lin_{}_timing.csv'.format(dataset)),
             ('SVM_RBF', './output/SVM_RBF_{}_timing.csv'.format(dataset)),
             ]

    df_fit = pd.DataFrame()
    df_predict = pd.DataFrame()
    for clf_type,f in files:
        df = pd.read_csv(f)
        df_fit[clf_type] = df['train']
        df_predict[clf_type] = df['test']

    for tset,df in [('fit',df_fit), ('predict',df_predict)]:
        # Plot
        plt.figure()
        plt.title(dataset + " " + tset + " Runtime")
        plt.xlabel("training samples (%)")
        plt.ylabel("time (s)")

        # box-like grid
        plt.grid()

        fracs = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
        plt.plot(fracs, df['DT'], '-', color="k", label="DT")
        plt.plot(fracs, df['Boost'], '--', color="k", label="Boost")
        plt.plot(fracs, df['KNN'], ':', color="k", label="KNN")
        plt.plot(fracs, df['ANN'], '-.', color="k", label="ANN")
        plt.plot(fracs, df['SVM_Lin'], '-', color="0.8", label="SVM_Lin")
        plt.plot(fracs, df['SVM_RBF'], '--', color="0.8", label="SVM_RBF")
        plt.legend(loc="best")

        outfile = './output/{}_{}_timing.tiff'.format(dataset, tset)
        plt.savefig(outfile, dpi=300, bbox_inches='tight')


class dtclf_pruned(dtclf):
    def remove_subtree(self,root):
        '''Clean up'''
        tree = self.tree_
        visited,stack = set(),[root]
        while stack:
            v = stack.pop()
            visited.add(v)
            left =tree.children_left[v]
            right=tree.children_right[v]
            if left >=0:
                stack.append(left)
            if right >=0:
                stack.append(right)
        for node in visited:
            tree.children_left[node] = -1
            tree.children_right[node] = -1


    def prune(self):
        assert(self.alpha >= -1)
        C = 1-self.alpha
        tree = self.tree_
        bestScore = self.score(self.valX,self.valY)
        candidates = np.flatnonzero(tree.children_left>=0)
        for candidate in reversed(candidates): # Go backwards/leaves up
            if tree.children_left[candidate]==tree.children_right[candidate]: # leaf node. Ignore
                continue
            left = tree.children_left[candidate]
            right = tree.children_right[candidate]
            tree.children_left[candidate]=tree.children_right[candidate]=-1
            score = self.score(self.valX,self.valY)
            if score >= C*bestScore:
                bestScore = score
                self.remove_subtree(candidate)
            else:
                tree.children_left[candidate]=left
                tree.children_right[candidate]=right
        assert (self.tree_.children_left>=0).sum() == (self.tree_.children_right>=0).sum()
        return self


    # def fit(self,X,Y):
    #     self.trgX, self.valX, self.trgY, self.valY = ms.train_test_split(X,
    #                                                                      Y,
    #                                                                      test_size=0.2,
    #                                                                      random_state=15,
    #                                                                      stratify=Y)

    #     super().fit(self.trgX,self.trgY)
    #     self.prune()
    #     return self


    def fit(self,X,Y,sample_weight=None,check_input=True, X_idx_sorted=None):
        if sample_weight is None:
            sample_weight = np.ones(X.shape[0])
        self.trgX = X.copy()
        self.trgY = Y.copy()
        self.trgWts = sample_weight.copy()
        sss = ms.StratifiedShuffleSplit(n_splits=1,test_size=0.2,random_state=15)
        for train_index, test_index in sss.split(self.trgX,self.trgY):
            self.valX = self.trgX[test_index]
            self.valY = self.trgY[test_index]
            self.trgX = self.trgX[train_index]
            self.trgY = self.trgY[train_index]
            self.valWts = sample_weight[test_index]
            self.trgWts = sample_weight[train_index]
        super().fit(self.trgX,self.trgY,self.trgWts,check_input,X_idx_sorted)
        self.prune()
        return self


    def __init__(self,
                 criterion="gini",
                 splitter="best",
                 max_depth=None,
                 min_samples_split=2,
                 min_samples_leaf=1,
                 min_weight_fraction_leaf=0.,
                 max_features=None,
                 random_state=None,
                 max_leaf_nodes=None,
                 # min_impurity_split=1e-7,
                 min_impurity_decrease=0,
                 class_weight=None,
                 presort=False,
                 alpha=0):

        super(dtclf_pruned, self).__init__(
            criterion=criterion,
            splitter=splitter,
            max_depth=max_depth,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_features=max_features,
            max_leaf_nodes=max_leaf_nodes,
            class_weight=class_weight,
            random_state=random_state,
            # min_impurity_split=min_impurity_split,
            min_impurity_decrease=min_impurity_decrease,
            presort=presort)
        self.alpha = alpha

    def numNodes(self):
        assert (self.tree_.children_left>=0).sum() == (self.tree_.children_right>=0).sum()
        return  (self.tree_.children_left>=0).sum()
