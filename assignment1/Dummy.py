#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-1.git
"""

import sys

import numpy as np
import pandas as pd
import sklearn

import sklearn.model_selection as ms
from sklearn.dummy import DummyClassifier

from helpers import basicResults, write_cm, write_learning_curve, makeTimingCurve, iterationLC


################################################################################
# Load Data, makes splits, create pipelines, fit data

neutrino_raw = pd.read_csv('MiniBooNE_PID.txt', sep='\s+', header=None)

neutrino = neutrino_raw.sample(20000)
neutrinoX = neutrino.iloc[:,:50].values.astype(np.float64)
neutrinoY = neutrino.iloc[:,50].values.astype(np.int)

neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY = ms.train_test_split(neutrinoX,
                                                                                 neutrinoY,
                                                                                 test_size=0.2,
                                                                                 random_state=16,
                                                                                 stratify=neutrinoY)

neutrino_clf = DummyClassifier()

write_cm(neutrino_clf,
         neutrino_trgX,
         neutrino_trgY,
         neutrino_tstX,
         neutrino_tstY,
         'Dummy',
         'neutrino')



connect4 = pd.read_csv('connect-4.data')
connect4['result'].replace('draw', 0, inplace=True)
connect4['result'].replace('win',  1, inplace=True)
connect4['result'].replace('loss', 2, inplace=True)

connect4X = pd.get_dummies(connect4.loc[:,'a1':'g6'], drop_first=True).values.astype(np.float64)
connect4Y = connect4['result'].values.astype(np.int)

connect4_trgX, connect4_tstX, connect4_trgY, connect4_tstY = ms.train_test_split(connect4X,
                                                                                 connect4Y,
                                                                                 test_size=0.2,
                                                                                 random_state=17,
                                                                                 stratify=connect4Y)


connect4_clf = DummyClassifier()

write_cm(connect4_clf,
         connect4_trgX,
         connect4_trgY,
         connect4_tstX,
         connect4_tstY,
         'Dummy',
         'connect4')
