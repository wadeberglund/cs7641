#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-1.git
"""

import sys

import numpy as np
import pandas as pd

import sklearn.model_selection as ms
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

from helpers import basicResults, write_cm, write_learning_curve, makeTimingCurve, iterationLC


################################################################################
# Load Data, makes splits, create pipelines, fit data

neutrino_raw = pd.read_csv('MiniBooNE_PID.txt', sep='\s+', header=None)

neutrino = neutrino_raw.sample(20000)
neutrinoX = neutrino.iloc[:,:50].values.astype(np.float64)
neutrinoY = neutrino.iloc[:,50].values.astype(np.int)

neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY = ms.train_test_split(neutrinoX,
                                                                                 neutrinoY,
                                                                                 test_size=0.2,
                                                                                 random_state=25,
                                                                                 stratify=neutrinoY)

pipeN = Pipeline([('Scale', StandardScaler()),
                  ('KNN', KNeighborsClassifier())])

# Neither Minkowski nor Mahalanobis provide better results
param_gridN = {'KNN__metric': ['manhattan','euclidean','chebyshev'],
               'KNN__n_neighbors': np.arange(1,15,2),
               'KNN__weights': ['uniform','distance']}

# neutrino_clf = basicResults(pipeN,
#                             neutrino_trgX,
#                             neutrino_trgY,
#                             neutrino_tstX,
#                             neutrino_tstY,
#                             param_gridN,
#                             'KNN',
#                             'neutrino')
# write_learning_curve(neutrino_clf.best_estimator_,
#                      neutrino_trgX,
#                      neutrino_trgY,
#                      'KNN',
#                      'neutrino')


connect4 = pd.read_csv('connect-4.data')
connect4X = pd.get_dummies(connect4.loc[:,'a1':'g6'], drop_first=True).values.astype(np.float64)
connect4['result'].replace('draw', 0, inplace=True)
connect4['result'].replace('win',  1, inplace=True)
connect4['result'].replace('loss', 2, inplace=True)
connect4Y = connect4['result'].values.astype(np.int)


connect4_trgX, connect4_tstX, connect4_trgY, connect4_tstY = ms.train_test_split(connect4X,
                                                                                 connect4Y,
                                                                                 test_size=0.2,
                                                                                 random_state=27,
                                                                                 stratify=connect4Y)


pipeC = Pipeline([#('Scale',StandardScaler()),
                  ('KNN', KNeighborsClassifier())])

param_gridC = {'KNN__metric':['manhattan','euclidean'],
               'KNN__n_neighbors':np.arange(1,21,4),
               'KNN__weights':['uniform','distance']}

# connect4_clf = basicResults(pipeC,
#                             connect4_trgX,
#                             connect4_trgY,
#                             connect4_tstX,
#                             connect4_tstY,
#                             param_gridC,
#                             'KNN',
#                             'connect4')
# write_learning_curve(connect4_clf.best_estimator_,
#                      connect4_trgX,
#                      connect4_trgY,
#                      'KNN',
#                      'connect4')


################################################################################
# Set final parameters and summarize data


# neutrino_final_params=connect4_clf.best_params_
neutrino_final_params={'KNN__n_neighbors': 9,
                       'KNN__weights': 'uniform',
                       'KNN__metric': 'manhattan'}
pipeN.set_params(**neutrino_final_params)

# Classification report and confusion matrix
write_cm(pipeN,
         neutrino_trgX,
         neutrino_trgY,
         neutrino_tstX,
         neutrino_tstY,
         'KNN',
         'neutrino')

# Learning curve
write_learning_curve(pipeN,
                     neutrino_trgX,
                     neutrino_trgY,
                     'KNN',
                     'neutrino')

# Timing statistics
makeTimingCurve(pipeN,
                neutrinoX,
                neutrinoY,
                'KNN',
                'neutrino')

# Performance when tuning the total number of estimators
iterationLC(pipeN,
            neutrino_trgX,
            neutrino_trgY,
            neutrino_tstX,
            neutrino_tstY,
            {'KNN__n_neighbors': np.arange(1,30,4)},
            'KNN',
            'neutrino',
            grid_search=False)


# connect4_final_params=connect4_clf.best_params_
connect4_final_params={'KNN__n_neighbors': 9,
                       'KNN__weights': 'distance',
                       'KNN__metric': 'manhattan'}
pipeC.set_params(**connect4_final_params)

# Classification report and confusion matrix
write_cm(pipeC,
         connect4_trgX,
         connect4_trgY,
         connect4_tstX,
         connect4_tstY,
         'KNN',
         'connect4')

# Learning curve
write_learning_curve(pipeC,
                     connect4_trgX,
                     connect4_trgY,
                     'KNN',
                     'connect4')

# Timing statistics
makeTimingCurve(pipeC,
                connect4X,
                connect4Y,
                'KNN',
                'connect4')

# Performance when tuning the total number of estimators
iterationLC(pipeC,
            connect4_trgX,
            connect4_trgY,
            connect4_tstX,
            connect4_tstY,
            {'KNN__n_neighbors': np.arange(1,30,4)},
            'KNN',
            'connect4',
            grid_search=False)

