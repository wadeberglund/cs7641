#!/usr/bin/env python3
"""
Based on the original code in this repository:
https://github.com/JonathanTay/CS-7641-assignment-1.git
"""

import sys

import numpy as np
import pandas as pd

import sklearn.model_selection as ms
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

from helpers import dtclf_pruned
from helpers import basicResults, write_cm, write_learning_curve, makeTimingCurve, iterationLC


################################################################################
# Load Data, makes splits, create pipelines, fit data

neutrino_raw = pd.read_csv('MiniBooNE_PID.txt', sep='\s+', header=None)

neutrino = neutrino_raw.sample(20000)
neutrinoX = neutrino.iloc[:,:50].values.astype(np.float64)
neutrinoY = neutrino.iloc[:,50].values.astype(np.int)

neutrino_trgX, neutrino_tstX, neutrino_trgY, neutrino_tstY = ms.train_test_split(neutrinoX,
                                                                                 neutrinoY,
                                                                                 test_size=0.2,
                                                                                 random_state=19,
                                                                                 stratify=neutrinoY)


connect4 = pd.read_csv('connect-4.data')
connect4X = pd.get_dummies(connect4.loc[:,'a1':'g6'], drop_first=True).values.astype(np.float64)
connect4['result'].replace('draw', 0, inplace=True)
connect4['result'].replace('win',  1, inplace=True)
connect4['result'].replace('loss', 2, inplace=True)
connect4Y = connect4['result'].values.astype(np.int)

connect4_trgX, connect4_tstX, connect4_trgY, connect4_tstY = ms.train_test_split(connect4X,
                                                                                 connect4Y,
                                                                                 test_size=0.2,
                                                                                 random_state=20,
                                                                                 stratify=connect4Y)


# Search for good alphas
alphas = [-1,
          -1e-1,
          -(1e-1)*10**-0.5,
          -1e-2,
          -(1e-2)*10**-0.5,
          -1e-3,
          -(1e-3)*10**-0.5,
          0,
          (1e-3)*10**-0.5,
          1e-3,
          (1e-2)*10**-0.5,
          1e-2,
          (1e-1)*10**-0.5,
          1e-1,
          ]


neutrino_base = dtclf_pruned(criterion='entropy',
                             alpha=3e-4,
                             max_depth=1,
                             class_weight='balanced',
                             random_state=21)

param_gridN = {'Boost__n_estimators': [10,20,30,45,60,80,100],
               'Boost__learning_rate': [0.1,0.2,0.3]}


# neutrino_base = DecisionTreeClassifier(random_state=21)

# param_gridN = {'Boost__n_estimators': [10,20,30,40,50,60,80,100,120],
#                'Boost__learning_rate': np.linspace(1e-16,0.5,11),
#                'Boost__base_estimator__criterion': ['entropy','gini'],
#                'Boost__base_estimator__max_depth': [10,12,14,16,18,20],
#                }

neutrino_booster = AdaBoostClassifier(algorithm='SAMME',
                                      base_estimator=neutrino_base,
                                      random_state=22)

pipeN = Pipeline([('Scale', StandardScaler()),
                  ('Boost', neutrino_booster)])

# neutrino_clf = basicResults(pipeN,
#                             neutrino_trgX,
#                             neutrino_trgY,
#                             neutrino_tstX,
#                             neutrino_tstY,
#                             param_gridN,
#                             'Boost',
#                             'neutrino')
# write_learning_curve(neutrino_clf.best_estimator_,
#                      neutrino_trgX,
#                      neutrino_trgY,
#                      'Boost',
#                      'neutrino')


# XXX Each fit takes about 6 minutes, 30 minutes for each 5-fold
connect4_base = dtclf_pruned(criterion='entropy',
                             alpha=0,
                             max_depth=10,
                             class_weight='balanced',
                             random_state=23)

# Anything with a learing rate of less than 0.1 is going to run forever.
param_gridC = {'Boost__n_estimators': [10,20,30,45,60,80,100],
               'Boost__learning_rate': [0.1,0.2,0.3]}

# connect4_base = DecisionTreeClassifier(max_depth=1,random_state=23)

# param_gridC = {
#                'Boost__n_estimators': [100,120,140,160,180],
#                'Boost__learning_rate': np.linspace(0.1,1.0,4),
#                'Boost__base_estimator__criterion': ['entropy','gini'],
#                'Boost__base_estimator__max_depth': [10,15,20],
#                }


connect4_booster = AdaBoostClassifier(algorithm='SAMME',
                                      base_estimator=connect4_base,
                                      random_state=24)


pipeC = Pipeline([#('Scale',StandardScaler()),
                  ('Boost',connect4_booster)])


# connect4_clf = basicResults(pipeC,
#                             connect4_trgX,
#                             connect4_trgY,
#                             connect4_tstX,
#                             connect4_tstY,
#                             param_gridC,
#                             'Boost',
#                             'connect4')
# write_learning_curve(connect4_clf.best_estimator_,
#                      connect4_trgX,
#                      connect4_trgY,
#                      'Boost',
#                      'connect4')


################################################################################
# Set final parameters and summarize data

# neutrino_final_params = neutrino_clf.best_params_
neutrino_final_params = {'Boost__n_estimators': 60,
                         'Boost__learning_rate': 0.1}
pipeN.set_params(**neutrino_final_params)


# Classification report and confusion matrix
write_cm(pipeN,
         neutrino_trgX,
         neutrino_trgY,
         neutrino_tstX,
         neutrino_tstY,
         'Boost',
         'neutrino')

# Learning curve
write_learning_curve(pipeN,
                     neutrino_trgX,
                     neutrino_trgY,
                     'Boost',
                     'neutrino')

# Timing statistics
makeTimingCurve(pipeN,
                neutrinoX,
                neutrinoY,
                'Boost',
                'neutrino')

# Performance when tuning the total number of estimators
iterationLC(pipeN,
            neutrino_trgX,
            neutrino_trgY,
            neutrino_tstX,
            neutrino_tstY,
            {'Boost__n_estimators': [1,2,5,10,20,30,40,50,60,80]},
            'Boost',
            'neutrino',
            grid_search=False)

# Regularization tuning. alpha=-1 has very low bias (will overfit) while larger values, especially positive ones, will
# fail to fit well.
iterationLC(pipeN,
            neutrino_trgX,
            neutrino_trgY,
            neutrino_tstX,
            neutrino_tstY,
            {'Boost__base_estimator__alpha': alphas},
            'Boost',
            'neutrino',
            grid_search=False,
            xscale='linear')


# connect4_final_params = connect4_clf.best_params_
connect4_final_params = {'Boost__n_estimators': 80,
                         'Boost__learning_rate': 0.1}
pipeC.set_params(**connect4_final_params)

# Classification report and confusion matrix
write_cm(pipeC,
         connect4_trgX,
         connect4_trgY,
         connect4_tstX,
         connect4_tstY,
         'Boost',
         'connect4')

# Learning curve
write_learning_curve(pipeC,
                     connect4_trgX,
                     connect4_trgY,
                     'Boost',
                     'connect4')

# Timing statistics
makeTimingCurve(pipeC,
                connect4X,
                connect4Y,
                'Boost',
                'connect4')

# Performance when tuning the total number of estimators
iterationLC(pipeC,
            connect4_trgX,
            connect4_trgY,
            connect4_tstX,
            connect4_tstY,
            {'Boost__n_estimators': [1,2,5,10,20,30,40,50,60,80]},
            'Boost',
            'connect4')

# Regularization tuning. alpha=-1 has very low bias (will overfit) while larger values, especially positive ones, will
# fail to fit well.
iterationLC(pipeC,
            connect4_trgX,
            connect4_trgY,
            connect4_tstX,
            connect4_tstY,
            {'Boost__base_estimator__alpha': alphas},
            'Boost',
            'connect4',
            grid_search=False,
            xscale='linear')
